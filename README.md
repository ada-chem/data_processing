# Data Processing

This repository contains the pipelines for processing data for Project Ada. This repository is broken into sub-packages that can be installed independently of eachother so that pipelines with conflicting requirements can be installed. The `data_processor` subpackage is the what is used to run the data pipelines. 

# How to:

Install the data processor subpackage:
```
pip install "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.data_processor&subdirectory=packages/data_processor"
```

In the `data_processor.py` script, you can find the `DataProcessing` method. This is used to process the desired pipeline. To run the `conductance_image_segmentation_01102020` pipeline, install the pipeline and call it by name in the `DataProcessing` method.


Intall the pipeline:
```
pip install "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.conductance_image_segmentation_01102020&subdirectory=packages/conductance_image_segmentation_01102020"
```

Call the pipeline in the data processor:
```
DataProcessing(
            pipeline_name='conductance_image_segmentation_01102020',
            campaign_directory=f'/path/to/campaign',
            results_directory=f'/path/to/results',
            # sample=[0], 
            workers=1,
            development=False,
            docker=False,
            docker_cache=False,
        )

```

If the sample is not specified, the processor will process all of the samples in the campaign folder.

If `docker` is set to `True`, the pipeline will be executed in a docker container that will be installed from gitlab. This is useful if you have conflicting dependencies in the pipelines.

Set `development` to `True` if you want to be able to develop new pipelines. This searches the relative subpackages if you have cloned the repository in-lieu of installing it.


