## Installing

```
pip install "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.adam_roughness_062022&subdirectory=packages/adam_roughness_062022"
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.adam_roughness_062022&subdirectory=packages/adam_roughness_062022"
```
