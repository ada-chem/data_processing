from data_processing.data_processor.luigi_task_wrapper import *
from luigi.mock import MockFileSystem
from data_processing.data_processor.pipe_line_base_fuctions import DataEntry, CustomTransformations, AggregateData, ProcessSampleParameters, CalculatePseudoconductivity, ProcessXRF, ProcessSpectroscopy, ProcessConductance, CampaignParameters, ProcessElectrolyticTestCellData, ProcessHaze
from data_processing.data_processor.imaging_base_functions import ProcessImageNoise


class LocalCampaignParameters(CampaignParameters):
    """
    This Task sets up the data processing pipeline. This task reads in the campaign parameters JSON and converts it into
    a chemical attributes CSV.
    """


class LocalProcessImageNoise(ProcessImageNoise):
    """
    This task analyzes roughness of the last image to be taken for one or
    more samples. The end result is a .CSV file that is saved in the results directory.
    """

    def requires(self):
        LoggingConfig.logger.info("passing control to LocalCampaignParameters")
        return LocalCampaignParameters()


class LocalProcessHaze(ProcessHaze):
    """
    This task analyzes haze at one or more positions for one or
    more samples. The end result is a .CSV file that is saved in the results directory.
    """

    def requires(self):
        LoggingConfig.logger.info("passing control to LocalCampaignParameters")
        return LocalCampaignParameters()


class LocalProcessSampleParameters(ProcessSampleParameters):
    """
    Get the sample parameters json in csv format for aggregation
    """

    def requires(self):
        LoggingConfig.logger.info("passing control to LocalCampaignParameters")
        return LocalCampaignParameters()


class LocalAggregateData(AggregateData):
    """
    Aggregate position and sample data.
    """

    def requires(self):
        LoggingConfig.logger.info("passing control to LocalProcessSampleParameters, LocalProcessHaze")
        yield [
            LocalProcessSampleParameters(),
            LocalProcessHaze(),
            LocalProcessImageNoise(),
        ]


class LocalCustomTransformations(CustomTransformations):

    def requires(self):
        LoggingConfig.logger.info("passing control to LocalAggregateData")
        return [
            LocalAggregateData(),
        ]


class LocalDataEntry(DataEntry):
    """
    Data is entered at the last task in the pipeline.
    """

    def requires(self):
        LoggingConfig.logger.info("passing control to LocalCustomTransformations")
        # passes the dir dictionary up to the preceding task.
        return LocalCustomTransformations()


def build_pipeline(parameter_dict):
    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    # Every luigi tasks inherits the processing parameters dictionary and the logger
    Parameters(dictionary=parameter_dict)
    LoggingConfig(campaign_results_directory=parameter_dict['campaign_results_directory'])
    LoggingConfig.logger.info("Building pipeline - passing control to LocalDataEntry")
    pipe = luigi.build(
        [
            LocalDataEntry(),
        ],
        local_scheduler=True,
        workers=parameter_dict['workers']
    )

    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')
    handlers = LoggingConfig.logger.handlers[:]
    for handler in handlers:
        LoggingConfig.logger.removeHandler(handler)
        handler.close()

    return pipe
