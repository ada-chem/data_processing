Pillow
luigi
pandas
numpy
pytest
hyperspy
git+https://gitlab.com/ada-chem/toolbox.git@master#egg=ada_toolbox
# git+https://gitlab.com/ada-chem/ada_imaging.git#egg=ada_imaging.crack_detection&subdirectory=packages/crack_detection
# git+https://gitlab.com/ada-chem/ada_imaging.git#egg=ada_imaging.film_homogen&subdirectory=packages/film_homogen
git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.data_processor&subdirectory=packages/data_processor