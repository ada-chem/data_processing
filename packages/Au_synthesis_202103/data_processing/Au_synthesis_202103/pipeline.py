from data_processing.data_processor.luigi_task_wrapper import *
from luigi.mock import MockFileSystem
from data_processing.data_processor.pipe_line_base_fuctions import DataEntry, CustomTransformations, AggregateData, ProcessSampleParameters, ProcessXRF, ProcessConductance, CampaignParameters


class LocalCampaignParameters(CampaignParameters):
    """
    This Task sets up the data processing pipeline. This task reads in the campaign parameters JSON and converts it into
    a chemical attributes CSV.
    """


class LocalProcessConductance(ProcessConductance):
    """
    This task calculates the conductance of the given data by position.
    """

    def requires(self):
        return LocalCampaignParameters()


class LocalProcessXRF(ProcessXRF):
    """
    This task analyzes Bruker M4 tornado XRF hypermaps at one or more positions for one or
    more samples. The end result is a .CSV file that is saved in the results directory.
    """

    def requires(self):
        return LocalCampaignParameters()


class LocalProcessSampleParameters(ProcessSampleParameters):
    """
    Get the sample parameters json in csv format for aggregation
    """

    def requires(self):
        return LocalCampaignParameters()


class LocalAggregateData(AggregateData):
    """
    Aggregate position and sample data.
    """

    def requires(self):
        yield [
            LocalProcessConductance(),
            LocalProcessXRF(),
            LocalProcessSampleParameters(),
        ]


class LocalCustomTransformations(CustomTransformations):

    def requires(self):
        return LocalAggregateData()


class LocalDataEntry(DataEntry):
    """
    Data is entered at the last task in the pipeline.
    """

    def requires(self):
        # passes the dir dictionary up to the preceding task.
        return LocalCustomTransformations()


def build_pipeline(parameter_dict):
    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    # Every luigi tasks inherits the processing parameters dictionary and the logger
    Parameters(dictionary=parameter_dict)
    LoggingConfig(campaign_results_directory=parameter_dict['campaign_results_directory'])

    pipe = luigi.build(
        [
            LocalDataEntry(),
        ],
        local_scheduler=True,
        workers=parameter_dict['workers']
    )

    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    return pipe
