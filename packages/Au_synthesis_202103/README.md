## Installing

```
pip install "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.Au_synthesis_2&subdirectory=packages/Au_synthesis_2"
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.Au_synthesis_2&subdirectory=packages/Au_synthesis_2"
```
