from data_processing.data_processor.luigi_task_wrapper import *
import data_processing.data_processor.luigi_helper_functions as lhf
from luigi.mock import MockFileSystem
import shutil
import os
import pandas as pd
from scipy import integrate
import numpy as np
from sklearn import linear_model
from numpy import (exp, finfo, float64, log, pi, sqrt)
from scipy.special import erf
from scipy.optimize import curve_fit

# Spectroscopy analysis
from ada_toolbox.analytical_lib import analytical_tools as m

pd.set_option('display.max_rows', 2000)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

log2 = log(2)
s2pi = sqrt(2 * pi)
spi = sqrt(pi)
s2 = sqrt(2.0)
tiny = finfo(float64).eps


def remove_unnamed(df):
    df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
    return df


def gaussian(x, amplitude=1.0, center=0.0, sigma=1.0):
    """Return a 1-dimensional Gaussian function.
    gaussian(x, amplitude, center, sigma) =
        (amplitude/(s2pi*sigma)) * exp(-(1.0*x-center)**2 / (2*sigma**2))
    """
    return amplitude * exp(-(x - center) ** 2 / sigma)


def skewed_gaussian(x, amplitude=1.0, center=0.0, sigma=1.0, gamma=0.0):
    """Return a Gaussian lineshape, skewed with error function.
    Equal to: gaussian(x, center, sigma)*(1+erf(beta*(x-center)))
    with beta = gamma/(sigma*sqrt(2))
    with  gamma < 0:  tail to low value of centroid
          gamma > 0:  tail to high value of centroid
    see https://en.wikipedia.org/wiki/Skew_normal_distribution
    """
    asym = 1 + erf(gamma * (x - center) / (s2 * sigma))
    gauss = gaussian(x, amplitude, center, sigma)
    result = asym * gauss
    return result


def laser_regions(df,
                  l1_low=390,
                  l1_high=420,
                  em_low=420,
                  em_high=750,
                  l2_low=750,
                  l2_high=830):
    l1_df = df[df['Wavelength [nm]'] >= l1_low]
    l1_df = l1_df[l1_df['Wavelength [nm]'] <= l1_high]

    em_df = df[df['Wavelength [nm]'] >= em_low]
    em_df = em_df[em_df['Wavelength [nm]'] <= em_high]

    l2_df = df[df['Wavelength [nm]'] >= l2_low]
    l2_df = l2_df[l2_df['Wavelength [nm]'] <= l2_high]

    return l1_df, em_df, l2_df


def em_model(df):
    X = df['Wavelength [nm]'].to_list()
    y = df['intensity_nb'].to_list()

    amplitude = max(y)
    amplitude_index = y.index(amplitude)
    center = df.iloc[amplitude_index]['Wavelength [nm]']
    sigma = np.std(y)
    gamma = 0

    popt, pcov = curve_fit(skewed_gaussian, X, y, p0=[amplitude, center, sigma, gamma])

    return popt


def predict_em(X, *popt):
    y_pred = skewed_gaussian(X, *popt)
    return y_pred


def remove_baseline(df):
    x = df['Wavelength [nm]'].values.reshape(-1, 1)
    y = df['Intensity'].values.reshape(-1, 1)

    model = linear_model.RANSACRegressor()
    model.fit(x, y)

    y_pred = model.predict(x).flatten()
    df['intensity_nb'] = df['Intensity'] - y_pred

    return df


def peak_wavelength(df):
    X = df['Wavelength [nm]'].to_list()
    y = df['intensity_nb'].to_list()
    amplitude = max(y)
    amplitude_index = y.index(amplitude)
    center = df.iloc[amplitude_index]['Wavelength [nm]']
    return center


def calculate_p(l1_df, em_df, l2_df, *em_pop):
    # Model the emission and remove its effect from L1 and L2
    p_l1 = l1_df['intensity_nb'] - predict_em(l1_df['Wavelength [nm]'], *em_pop)
    p_em = em_df['intensity_nb']
    p_l2 = l2_df['intensity_nb'] - predict_em(l2_df['Wavelength [nm]'], *em_pop)

    p = np.concatenate((p_l1, p_em, p_l2), axis=None)
    p_wave = np.concatenate((l1_df['Wavelength [nm]'], em_df['Wavelength [nm]'], l2_df['Wavelength [nm]']), axis=None)

    p_l1 = pd.DataFrame({'Wavelength [nm]': l1_df['Wavelength [nm]'], 'p': p_l1})
    p_em = pd.DataFrame({'Wavelength [nm]': em_df['Wavelength [nm]'], 'p': p_em})
    p_l2 = pd.DataFrame({'Wavelength [nm]': l2_df['Wavelength [nm]'], 'p': p_l2})
    p_df = pd.DataFrame({'Wavelength [nm]': p_wave, 'p': p})

    return p_l1, p_em, p_l2, p_df


class CampaignParameters(TaskTemplate):
    """
    This Task sets up the data processing pipeline. This task reads in the campaign parameters JSON and converts it into
    a chemical attributes CSV.
    """

    def run(self):

        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self._get_classname()} - Reading Campaign Parameters")

        # instantiate dict keys
        self.parameters_dict['aggregate_data'] = {}  # Cost function builder looks here for data
        self.parameters_dict['raw_data'] = {}
        self.parameters_dict['sample_data'] = {}
        self.parameters_dict['position_data'] = {}
        self.parameters_dict['is_spectrometer'] = {}
        self.parameters_dict['uv_spectrometer'] = {}
        self.parameters_dict['sample_directories'] = {}

        # Get sample directories in the raw data folder
        campaign_files = os.listdir(self.parameters_dict['campaign_directory'])
        requested_samples = self.parameters_dict.get('sample', None)

        # if labels.json exists save the output
        p = os.path.join(self.parameters_dict.get('campaign_directory', ''), 'labels.json')
        if os.path.exists(p):
            labels = os.path.join(self.parameters_dict['processed_data_directory'], "labels.json")
            self.parameters_dict['labels'] = labels
            shutil.copy(p, labels)

        for file in campaign_files:

            file_directory = os.path.join(self.parameters_dict['campaign_directory'], file)

            if file.startswith('sample_') and \
                    os.path.isdir(file_directory) and \
                    len(os.listdir(file_directory)) >= 1:

                file_number = int(file.split("_")[1])

                if requested_samples is None:
                    self.parameters_dict['sample_directories'][file_number] = file_directory

                else:
                    if file_number in requested_samples:
                        self.parameters_dict['sample_directories'][file_number] = file_directory
                    else:
                        pass

        self.save_parameters_dict()


class PLQE(TaskTemplate):
    """
    This task runs the image classification algorithms on the sample images.
    """

    def requires(self):
        return CampaignParameters()

    def run(self):

        self.get_parameters_dict()
        LoggingConfig.logger.info(f"{self._get_classname()} - Processing Integrating Sphere Spectroscopy")

        plqe_df_list = []

        try:
            # Create a CSV of the experiment dictionary for each sample.
            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():
                sample_number = int(sample_number)

                if 'IRSPEC' not in os.listdir(sample_directory):
                    raise Exception('IRSPEC directory doesnt exist')
                else:
                    irspec_directory = os.path.join(sample_directory, 'IRSPEC')

                irspec_all_pos = [file for file in os.listdir(irspec_directory) if file.endswith('.csv')]

                # get number  of unique positions
                positions = [int((pos.split('.')[0]).split('_')[1]) for pos in irspec_all_pos]
                positions = list(set(positions))
                positions.sort()

                for position in positions:
                    no_pos_dir = [os.path.join(irspec_directory, pos) for pos in irspec_all_pos if
                                  int((pos.split('.')[0]).split('_')[1]) == position and pos.startswith('NO')][0]

                    on_pos_dir = [os.path.join(irspec_directory, pos) for pos in irspec_all_pos if
                                  int((pos.split('.')[0]).split('_')[1]) == position and pos.startswith('ON')][0]

                    off_pos_dir = [os.path.join(irspec_directory, pos) for pos in irspec_all_pos if
                                   int((pos.split('.')[0]).split('_')[1]) == position and pos.startswith('OFF')][0]

                    # The sample is in the laser and spectrometer is read
                    df_ON = remove_unnamed(pd.read_csv(on_pos_dir))
                    on_nb = remove_baseline(df_ON)
                    on_l1_df, on_em_df, on_l2_df = laser_regions(on_nb)
                    on_em_model = em_model(on_em_df)
                    on_p_l1, on_p_em, on_p_l2, on_p_df = calculate_p(on_l1_df, on_em_df, on_l2_df, *on_em_model)

                    # The sample is in the integrating sphere but off of the laser
                    df_OFF = remove_unnamed(pd.read_csv(off_pos_dir))
                    off_nb = remove_baseline(df_OFF)
                    off_l1_df, off_em_df, off_l2_df = laser_regions(off_nb)
                    off_em_model = em_model(off_em_df)
                    off_p_l1, off_p_em, off_p_l2, off_p_df = calculate_p(off_l1_df, off_em_df, off_l2_df, *off_em_model)

                    # No sample is in the spectrometer. Measures an empty integrating sphere.
                    df_NO = remove_unnamed(pd.read_csv(no_pos_dir))
                    no_nb = remove_baseline(df_NO)
                    no_l1_df, no_em_df, no_l2_df = laser_regions(no_nb)

                    l1_on_area = integrate.trapz(on_p_l1['p'], x=on_p_l1['Wavelength [nm]'])
                    l1_off_area = integrate.trapz(off_p_l1['p'], x=off_p_l1['Wavelength [nm]'])
                    l1_no_area = integrate.trapz(no_l1_df['intensity_nb'], x=no_l1_df['Wavelength [nm]'])

                    p_on = integrate.trapz(on_p_df['p'], x=on_p_df['Wavelength [nm]'])
                    p_off = integrate.trapz(off_p_df['p'], x=off_p_df['Wavelength [nm]'])

                    lambda_em_on = peak_wavelength(on_em_df)
                    lambda_l1_on = peak_wavelength(on_l1_df)

                    absorption = 1 - (l1_on_area / l1_off_area)

                    plqe = (p_on - (1 - absorption) * p_off) / (l1_no_area * absorption)

                    plqe_df = pd.DataFrame({'sample': [sample_number],
                                            'position': [position],
                                            'plqe': [plqe],
                                            'lambda_em': [lambda_em_on],
                                            'lambda_max': [lambda_l1_on]})
                    plqe_df_list.append(plqe_df)

            plqe_df_total = pd.concat(plqe_df_list)

            # Save the data
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving position PLQE data")
            prcocessed_plqe_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                     "position_plqe.csv")
            plqe_df_total.to_csv(prcocessed_plqe_directory, index=False)
            self.parameters_dict['position_data']['plqe'] = prcocessed_plqe_directory

            self.save_parameters_dict()

        except Exception as e:
            print(e)
            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")


class SpectralRatio(TaskTemplate):
    '''
    This task processes the spectroscopy data by position. To calculate the height of the gaussian for a sample, we must
    process 4 files: blank reflectance, blank transmission, reflectance, and transmission.
    Blank reflectance and transmission are only found in the first sample for a given campaign. As such, if we are only
    processing a single campaign, we must back track through the samples to find the blank reflectance and transmission
    data.
    '''

    def requires(self):
        return PLQE()

    def run(self):

        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self._get_classname()} - Processing Spectroscopy")

        sr_df_list = []
        spectroscopy_ctrl_index = self.parameters_dict.get('spectroscopy_ctrl_index', None)
        plqe_df = pd.read_csv(self.parameters_dict['position_data']['plqe'])

        try:
            # Create a CSV of the experiment dictionary for each sample.
            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():
                sample_number = int(sample_number)
                blank_spectra = {}

                sample_plqe_df = plqe_df[plqe_df['sample'] == sample_number]

                if 'UVVIS' not in os.listdir(sample_directory):
                    raise Exception('UVVIS directory doesnt exist')
                else:
                    spectra_directory = os.path.join(sample_directory, 'UVVIS')

                blank_files_all_pos = [file for file in os.listdir(spectra_directory) if
                                       'blank' in file and file.endswith('.csv')]
                blank_files_all_pos.sort()

                # determine which files to use for calculations
                if spectroscopy_ctrl_index is not None:
                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Using control index to determine which spectroscopy file to use for calculations")
                    spectra_files_all_pos = [file for file in os.listdir(spectra_directory) if file.startswith(
                        '{:03d}'.format(spectroscopy_ctrl_index)) and 'blank' not in file and file.endswith('.csv')]
                else:
                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - No control index given - using the default file")
                    spectra_files_all_pos = [file for file in os.listdir(spectra_directory) if
                                             'blank' not in file and file.endswith('.csv')]

                spectra_files_all_pos.sort()

                # get number  of unique positions
                positions = [lhf.file_name_parser(filename)[3] for filename in spectra_files_all_pos]
                positions = list(set(positions))
                positions.sort()

                for position in positions:
                    position_number = int(position)

                    pos_plqe_df = sample_plqe_df[sample_plqe_df['position'] == position_number]

                    blank_spectra[position_number] = {}

                    spectra_pos = [file for file in spectra_files_all_pos if
                                   lhf.file_name_parser(file)[3] == position]

                    spectra_blank_pos = [file for file in blank_files_all_pos if
                                         lhf.file_name_parser(file)[3] == position]

                    if not spectra_blank_pos:
                        raise Exception('Blank slide data NOT FOUND in sample directory')

                    elif len(spectra_pos) != len(spectra_blank_pos):
                        raise ValueError('Missmatched number of blank spectra files and sample spectra files')

                    elif len(spectra_pos) != 2:
                        raise Exception(f"Must have a single reflectance file and single transmission data file.")

                    for file in spectra_blank_pos:
                        if '_t' in file:
                            blank_transmission_directory = os.path.join(spectra_directory, file)
                            blank_transmission = pd.read_csv(blank_transmission_directory)
                            blank_transmission['sample'] = sample_number
                            blank_transmission['position'] = position_number
                            blank_transmission['r-t'] = 't'
                            blank_transmission['blank'] = True

                            blank_spectra[position_number]['t'] = blank_transmission
                        else:
                            blank_reflectance_directory = os.path.join(spectra_directory, file)
                            blank_reflectance = pd.read_csv(blank_reflectance_directory)
                            blank_reflectance['sample'] = sample_number
                            blank_reflectance['position'] = position_number
                            blank_reflectance['r-t'] = 'r'
                            blank_reflectance['blank'] = True

                            blank_spectra[position_number]['r'] = blank_reflectance

                    if blank_spectra is None:
                        raise Exception(f"Missing blank reflectance or transmission data in SAMPLE {sample_number}")

                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Processing Spectroscopy for sample {sample_number}, position {position_number}")

                    for file in spectra_pos:
                        if '_t' in file:
                            transmission_directory = os.path.join(spectra_directory, file)
                            transmission = pd.read_csv(transmission_directory)
                            transmission['sample'] = sample_number
                            transmission['position'] = position_number
                            transmission['r-t'] = 't'
                            transmission['blank'] = False
                        else:
                            reflectance_directory = os.path.join(spectra_directory, file)
                            reflectance = pd.read_csv(reflectance_directory)
                            reflectance['sample'] = sample_number
                            reflectance['position'] = position_number
                            reflectance['r-t'] = 'r'
                            reflectance['blank'] = False

                    r_0 = blank_reflectance['calibrated_signal']
                    t_0 = blank_transmission['calibrated_signal']
                    r_1 = reflectance['calibrated_signal']
                    t_1 = transmission['calibrated_signal']
                    x_nm = reflectance['x_(nm)']

                    lambda_em = pos_plqe_df.iloc[0]['lambda_em']
                    lambda_max = pos_plqe_df.iloc[0]['lambda_max']

                    # Calculate absorbance of film
                    absorbance_df = m.calculate_film_absorbance(r_0, t_0, r_1, t_1, x_nm)
                    absorbance_df = absorbance_df.dropna(subset=['A_f'])
                    absorbance_df = absorbance_df[absorbance_df['x_(nm)'] >= 220]

                    # Get the locations of peak emmission and max peak
                    lambda_absorbance_df = absorbance_df[['x_(nm)', 'log_A_f']]
                    lambda_df = pd.DataFrame({'x_(nm)': [lambda_em, lambda_max],
                                              'log_A_f': [None, None]})
                    lambda_absorbance_df = pd.concat([lambda_df, lambda_absorbance_df]).sort_values(
                        by=['x_(nm)']).reset_index(drop=True)
                    lambda_absorbance_df = lambda_absorbance_df.interpolate()

                    log_a_f_emission = lambda_absorbance_df[lambda_absorbance_df['x_(nm)'] == lambda_em].iloc[0][
                        'log_A_f']
                    log_a_f_max = lambda_absorbance_df[lambda_absorbance_df['x_(nm)'] == lambda_max].iloc[0]['log_A_f']

                    SR = log_a_f_emission / log_a_f_max

                    SR_df = pd.DataFrame({'sample': [sample_number],
                                          'position': [position_number],
                                          'log_a_f_emission': [log_a_f_emission],
                                          'log_a_f_max': [log_a_f_max],
                                          'sr': [SR]})

                    sr_df_list.append(SR_df)

            # Aggregate raw and processed data
            sr_df_total = pd.concat(sr_df_list)

            # Save the data
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving position spectral ratio")
            spectral_ratio_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                    "position_spectral_ratio.csv")
            sr_df_total.to_csv(spectral_ratio_directory, index=False)
            self.parameters_dict['position_data']['spectral_ratio'] = spectral_ratio_directory

            self.save_parameters_dict()

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")


class CalculateResponse(TaskTemplate):
    '''
    This task combines the spectroscopy and conductivity subpipes. Pseudomobility for a sample is calculated by getting
    the pseudomobility by position and then taking the average of the positions for the sample.
    '''

    def requires(self):
        return SpectralRatio()

    def run(self):
        self.get_parameters_dict()

        # Get the processed spectroscopy and conducutivity dataframe directories
        spectral_ratio_directory = self.parameters_dict['position_data'].get('spectral_ratio', None)
        plqe_directory = self.parameters_dict['position_data'].get('plqe', None)

        if spectral_ratio_directory is not None:
            sr_df = pd.read_csv(spectral_ratio_directory)

        if plqe_directory is not None:
            plqe_df = pd.read_csv(plqe_directory)

        try:
            LoggingConfig.logger.info(f"{self._get_classname()} - Calculating response")

            # Merge the conductance and height of the gaussian dataframes by position and sample.
            position_df = pd.merge(sr_df, plqe_df, on=['sample', 'position'])

            # Calculate positional pseudomobility
            position_df['response'] = position_df['plqe'] / position_df['sr']

            # Calculate sample pseudomobility by taking the mean for a given sample.
            sample_df = position_df.groupby('sample').aggregate([np.mean, np.std])
            del sample_df['position']
            sample_df.columns = sample_df.columns.map('_'.join).str.strip('_')
            sample_df.reset_index(level=0, inplace=True)

            # Save the processed sample pseudomobility data
            sample_response_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                     'sample_response.csv')
            sample_df.to_csv(sample_response_directory, index=False)
            self.parameters_dict['aggregate_data']['sample'] = sample_response_directory
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving sample response data")

            # Save the processed positional pseudomobility data
            position_response_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                       'position_response.csv')
            position_df.to_csv(position_response_directory, index=False)
            self.parameters_dict['aggregate_data']['position'] = position_response_directory
            self.parameters_dict['position_data']['response'] = position_response_directory
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving position response data")

            self.save_parameters_dict()

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")


class DataEntry(TaskTemplate):
    '''
    Data is entered at the last task in the pipeline.
    '''

    def requires(self):
        # passes the dir dictionary up to the preceding task.
        return CalculateResponse()

    def run(self):
        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self._get_classname()} - Complete the pipeline")

        self.save_parameters_dict()


def build_pipeline(parameter_dict):
    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    # Every luigi tasks inherits the processing parameters dictionary and the logger
    Parameters(dictionary=parameter_dict)
    LoggingConfig(campaign_results_directory=parameter_dict['campaign_results_directory'])

    pipe = luigi.build(
        [
            DataEntry(),
        ],
        local_scheduler=True,
        workers=parameter_dict['workers']
    )

    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    return pipe
