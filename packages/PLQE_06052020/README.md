## Installing

```
pip install "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.PLQE_06052020&subdirectory=packages/PLQE_06052020"
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.PLQE_06052020&subdirectory=packages/PLQE_06052020"
```
