from setuptools import setup

PACKAGE = 'data_processor'
NAME = f'data_processing.{PACKAGE}'

about = {}
with open(f'data_processing/{PACKAGE}/__about__.py') as fp:
    exec(fp.read(), about)

with open("README.md", "r") as fh:
    DESCRIPTION = fh.read()

INSTALL_REQUIRES = [
    "luigi",
    "pandas",
    "numpy",
]

setup(
    name=NAME,
    version=about["__version__"],
    install_requires=INSTALL_REQUIRES,
    namespace_packages=['data_processing'],
    packages=[NAME],
    zip_safe=False,
    include_package_data=True,
    package_data={NAME: ['docker/**/*']},
)
