## Installing

```
pip install "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.data_processor&subdirectory=packages/data_processor"
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.data_processor&subdirectory=packages/data_processor"
```
