import logging
import os
import pickle
from ada_tools.tcpip_template import Server
from data_processing.data_processor.data_processor import DataProcessing

logging.basicConfig(level="INFO")

logger = logging.getLogger(__name__)

SERVER_ADDRESS = (os.environ['COMPUTERNAME'], 1055)  # address and port of xrf server

SERVER_ADDRESS = ("142.103.143.57", 1055)
# encoding/decoding functions
def serialise(obj):
    return pickle.dumps(obj)


def deserialise(b):
    return pickle.loads(b)


class DataProcssor:
    def __init__(self):
        pass

    def process_data(self, pipeline_name, campaign_directory, results_directory, sample_number):
        DataProcessing(
            development=True,
            pipeline_name=pipeline_name,
            campaign_directory=campaign_directory,
            results_directory=results_directory,
            sample=[sample_number],
            workers=1,
            docker=False,
            docker_cache=True,
        )


class DataProcessorServer(Server):
    def __init__(self, server_address=None):
        # create logger
        self.__dict__["logger"] = logger.getChild(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)
        logger.info(f"Start XRFServer")
        datap = DataProcssor()
        super().__init__(station=datap, server_address=server_address)


if __name__ == '__main__':
    xrf_server = DataProcessorServer(server_address=SERVER_ADDRESS)
