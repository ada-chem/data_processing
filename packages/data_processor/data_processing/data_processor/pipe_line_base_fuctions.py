from data_processing.data_processor.luigi_task_wrapper import *
import data_processing.data_processor.cost_function_builder as cfb
import data_processing.data_processor.luigi_helper_functions as lhf
import pandas as pd
import numpy as np
from functools import reduce
import shutil
import glob
import os
import pathlib

# Profilometry analysis
from ada_toolbox.data_analysis_lib import profilometry_analysis as profilometry
from scipy.signal import find_peaks, peak_prominences

from sklearn.neighbors import KernelDensity
from data_processing.data_processor.hyperspec import HyperPos

# Spectroscopy analysis
from ada_toolbox.analytical_lib import analytical_tools as m

# Conductivity analysis
from ada_toolbox.data_analysis_lib import conductivity_analysis as cond

# Imaging analysis
import warnings
from ada_toolbox.data_analysis_lib import image_analysis as img_analysis

# electrolytic stability
from scipy import integrate

# ignore tensorflow warnings
warnings.filterwarnings('ignore', module=".*tensorflow.*")

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


def get_largest_numbered_file_name(file_path, ext=None):
    if ext is None:
        ext = "*"
    else:
        ext = ext.strip(".")
    list_of_image_file_names = glob.glob(os.path.join(file_path, "*."+ext))

    largest_file_number = -1
    largest_file_name = ""

    for image_file_name in list_of_image_file_names:
        basename = os.path.basename(image_file_name)
        basename_noext = pathlib.Path(basename).stem
        name_split = basename_noext.split("_")
        last_split = name_split[-1]
        try:
            file_number = int(last_split)
        except ValueError:
            file_number = 0

        if file_number > largest_file_number:
            largest_file_number = file_number
            largest_file_name = image_file_name

    return largest_file_name


def get_smallest_numbered_file_name(file_path, ext=None):
    if ext is None:
        ext = "*"
    else:
        ext = ext.strip(".")
    list_of_image_file_names = glob.glob(os.path.join(file_path, "*."+ext))

    smallest_file_number = 1000000
    smallest_file_name = ""

    for image_file_name in list_of_image_file_names:
        basename = os.path.basename(image_file_name)
        basename_noext = pathlib.Path(basename).stem
        name_split = basename_noext.split("_")
        last_split = name_split[-1]
        try:
            file_number = int(last_split)
        except ValueError:
            file_number = 0

        if file_number < smallest_file_number:
            smallest_file_number = file_number
            smallest_file_name = image_file_name

    return smallest_file_name

class CampaignParameters(TaskTemplate):
    """
    This Task sets up the data processing pipeline. This task reads in the campaign parameters JSON and converts it into
    a chemical attributes CSV.
    """

    def run(self):

        self.get_parameters_dict()
        LoggingConfig.logger.info("running CampaignParameters")
        LoggingConfig.logger.info(f"{self._get_classname()} - Reading Campaign Parameters")

        # instantiate dict keys
        self.parameters_dict['raw_data'] = {}
        self.parameters_dict['sample_data'] = {}
        self.parameters_dict['position_data'] = {}
        self.parameters_dict['aggregate_data'] = {}
        self.parameters_dict['image_directory'] = {}
        self.parameters_dict['sample_directories'] = {}
        self.parameters_dict['processed_image_directory'] = {}

        # Get sample directories in the raw data folder
        campaign_files = os.listdir(self.parameters_dict['campaign_directory'])
        requested_samples = self.parameters_dict.get('sample', None)

        if "config.json" in campaign_files:
            file_directory = os.path.join(self.parameters_dict['campaign_directory'], "config.json")
            self.parameters_dict['config_directory'] = file_directory
        else:
            self.parameters_dict['config_directory'] = None

        if "campaign_parameters.json" in campaign_files:
            file_directory = os.path.join(self.parameters_dict['campaign_directory'], "campaign_parameters.json")
            self.parameters_dict['campaign_parameters_directory'] = file_directory
        else:
            self.parameters_dict['campaign_parameters_directory'] = None

        # if labels.json exists save the output
        p = os.path.join(self.parameters_dict.get('campaign_directory', ''), 'labels.json')
        if os.path.exists(p):
            labels = os.path.join(self.parameters_dict['processed_data_directory'], "labels.json")
            self.parameters_dict['labels'] = labels
            shutil.copy(p, labels)

        for file in campaign_files:

            file_directory = os.path.join(self.parameters_dict['campaign_directory'], file)

            if file.startswith('sample_') and \
                    os.path.isdir(file_directory) and \
                    len(os.listdir(file_directory)) >= 1:

                file_number = int(file.split("_")[1])

                if requested_samples is None:
                    self.parameters_dict['sample_directories'][file_number] = file_directory

                else:

                    if file_number in requested_samples:
                        self.parameters_dict['sample_directories'][file_number] = file_directory

                    else:
                        pass

        self.save_parameters_dict()


class ProcessImages(TaskTemplate):
    """
    This task runs the image classification algorithms on the sample images.
    """

    def requires(self):
        return CampaignParameters()

    def run(self):

        self.get_parameters_dict()
        LoggingConfig.logger.info("running ProcessImages")
        image_scores_list = []
        image_ctrl_index = self.parameters_dict.get('image_ctrl_index', None)

        try:
            # Create a CSV of the experiment dictionary for each sample. Map JSON to DF.
            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():
                sample_number = int(sample_number)
                if 'IMAGES' not in os.listdir(sample_directory):
                    raise Exception('IMAGES directory doesnt exist')
                else:
                    images_directory = os.path.join(sample_directory, 'IMAGES')

                image_files = os.listdir(images_directory)
                image_files = [file for file in image_files if file.endswith('.jpg')]
                image_files.sort()

                self.parameters_dict['image_directory'][sample_number] = {}
                self.parameters_dict['processed_image_directory'][sample_number] = {}

                LoggingConfig.logger.info(
                    f"{self._get_classname()} - Processing Images for sample {sample_number}")
                if image_ctrl_index is not None:
                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Using control index to determine which image file to use for processing")
                    image_files = [file for file in image_files if file.startswith(
                        '{:03d}'.format(image_ctrl_index))]
                else:
                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - No control index given - using the last file in the list - {image_files[-1]}")
                    image_files = [image_files[-1]]

                for image in image_files:
                    # parse filename
                    ctrl_index, equipment, other, position = lhf.file_name_parser(image)

                    image_file = os.path.join(images_directory, image)
                    image_scores = {}

                    # CNN image processing
                    classifier = ImageClassifier()
                    for model_name in classifier.models.keys():
                        LoggingConfig.logger.info(
                            f"{self._get_classname()} - Generating {model_name} score for image: {image_file}")
                        image_scores[f'{model_name}_score'] = float(classifier.label_image(model_name, image_file)[0])

                    # Homogeneity Scores
                    processed_image_dir = os.path.join(self.parameters_dict['processed_data_directory'],
                                                       f'processed_image_s{sample_number}_m{ctrl_index}.jpg')
                    shape_percent, shape_quality = film_quality(image=image_file,
                                                                output=processed_image_dir)
                    image_scores['shape_percent'] = shape_percent
                    image_scores['shape_quality_percent'] = shape_quality

                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Generating blur and brightness scores for image: {image_file}")
                    image_scores['blur_score'] = img_analysis.blur_detection(image_directory=image_file)
                    image_scores['brightness_score'] = img_analysis.brightness_detection(image_directory=image_file)
                    image_scores['sample'] = int(sample_number)
                    image_scores['position'] = int(position)

                    self.parameters_dict['image_directory'][sample_number][ctrl_index] = image_file
                    self.parameters_dict['processed_image_directory'][sample_number][ctrl_index] = processed_image_dir

                    # Convert the dictionary to a dataframe and save as CSV
                    image_scores_df = pd.DataFrame.from_dict(image_scores, orient='index')
                    image_df_transpose = image_scores_df.T
                    image_scores_list.append(image_df_transpose)

            # Save position scores
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving position image data")
            image_df_total = pd.concat(image_scores_list, ignore_index=True)
            image_scores_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                  "position_image_scores.csv")
            image_df_total.to_csv(image_scores_directory, index=False)
            self.parameters_dict['position_data']['image_scores'] = image_scores_directory

            # Save sample scores
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving sample image data")
            sample_df = image_df_total.groupby(['sample']).mean()
            sample_df.reset_index(level=0, inplace=True)
            del sample_df['position']
            image_sample_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                  "sample_image_scores.csv")
            sample_df.to_csv(image_sample_directory, index=False)
            self.parameters_dict['sample_data']['image_scores'] = image_sample_directory

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} -  processing failed")

        self.save_parameters_dict()


class ProcessConductance(TaskTemplate):
    """
    This task calculates the conductance of the given data by position.
    """

    def requires(self):
        return CampaignParameters()

    def run(self):

        self.get_parameters_dict()
        LoggingConfig.logger.info("running ProcessConductance")
        LoggingConfig.logger.info(f"{self._get_classname()} - Processing Conductivity")

        raw_conductivity_list = []
        processed_conductivity_list = []
        units = {}

        try:
            # Create a CSV of the experiment dictionary for each sample. Map JSON to DF.
            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():
                sample_number = int(sample_number)

                if 'CONDUCTIVITY' not in os.listdir(sample_directory):
                    raise Exception('CONDUCTIVITY directory doesnt exist')
                else:
                    conductivity_directory = os.path.join(sample_directory, 'CONDUCTIVITY')

                LoggingConfig.logger.info(
                    f"{self._get_classname()} - Processing Conductivity for sample {sample_number}")

                conductivity_position_files = os.listdir(conductivity_directory)
                conductivity_position_files = [file for file in conductivity_position_files if file.endswith('.csv')]
                conductivity_position_files.sort()

                for file in conductivity_position_files:
                    ctrl_index, equipment, other, position = lhf.file_name_parser(file)
                    position_number = int(position)

                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Processing conductivity for sample {sample_number}, position {position_number}")

                    conductivity_file_directory = os.path.join(conductivity_directory, file)
                    conductivity_df = pd.read_csv(conductivity_file_directory)
                    conductivity_df['sample'] = sample_number
                    conductivity_df['position'] = position_number

                    # Aggregate all raw positional data
                    raw_conductivity_list.append(conductivity_df)

                    # process the conductivity data
                    # Remove saturated data points. Saturation thresholds are defined in the toolbox.
                    df_rs, mask = cond.remove_saturated_points(data=conductivity_df)

                    # fit resistance line
                    r2, slope, intercept, inliers = cond.linear_regression_RANSAC(
                        data=df_rs,
                        x_axis="Current",
                        y_axis="Voltage",
                    )

                    # check intercept is near 0
                    if slope != 0:
                        thresh = 1  # volts
                        if abs(intercept) > thresh:
                            LoggingConfig.logger.info(
                                f"{self._get_classname()} - y intercept: {intercept} > {thresh} setting to conductance to 0")
                            slope = 0

                    # convert to conductance
                    try:
                        conductance = 1 / slope
                    except ZeroDivisionError:
                        conductance = 0

                    processed_dict = {
                        'sample': sample_number,
                        'position': position_number,
                        'conductance': conductance,
                    }

                    units['r2'] = {'r2': 'siemens'}
                    units['intercept'] = {'intercept': 'siemens'}
                    units['conductance'] = {'conductance': 'siemens'}

                    processed_df = pd.DataFrame.from_dict(processed_dict, orient='index')
                    transpose_processed_df = processed_df.T
                    processed_conductivity_list.append(transpose_processed_df)

            # Combine the dataframes from each sample and save the data
            raw_conductivity_total = pd.concat(raw_conductivity_list)
            processed_conductivity_total = pd.concat(processed_conductivity_list)

            # Make the sample and position values ints from floats.
            processed_conductivity_total['sample'] = processed_conductivity_total['sample'].apply(np.int64)
            processed_conductivity_total['position'] = processed_conductivity_total['position'].apply(np.int64)

            LoggingConfig.logger.info(f"{self._get_classname()} - Saving position conductivity data")
            processed_conductivity_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                            "position_conductivity.csv")
            processed_conductivity_total.to_csv(processed_conductivity_directory, index=False)
            self.parameters_dict['position_data']['conductivity'] = processed_conductivity_directory

            # Save sample conductivity
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving sample conductivity data")
            processed_sample_conductivity_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                                   "sample_conductivity.csv")
            processed_conductivity_total[['sample', 'position']] = processed_conductivity_total[
                ['sample', 'position']].astype(int)
            conductance_dicts = []
            for sample in np.unique(processed_conductivity_total['sample']):
                sample_df = processed_conductivity_total[processed_conductivity_total['sample'] == sample]
                conductance_array = np.asarray(sample_df['conductance'])
                conductance_mean = conductance_array.mean()
                conductance_std = conductance_array.std()
                conductance_mean_nz = conductance_array[np.nonzero(conductance_array)].mean()
                conductance_std_nz = conductance_array[np.nonzero(conductance_array)].std()
                conductive_fraction = len(conductance_array[np.nonzero(conductance_array)]) / len(conductance_array)

                conductance_dict = {
                    'sample': sample,
                    'conductance_mean': conductance_mean,
                    'conductance_std': conductance_std,
                    'conductance_mean_nz': conductance_mean_nz,
                    'conductance_std_nz': conductance_std_nz,
                    'conductive_fraction': conductive_fraction,
                }
                conductance_dicts.append(conductance_dict)

            samples_df = pd.DataFrame(conductance_dicts)
            samples_df.to_csv(processed_sample_conductivity_directory, index=False)
            self.parameters_dict['sample_data']['conductivity'] = processed_sample_conductivity_directory

            LoggingConfig.logger.info(f"{self._get_classname()} - Saving raw conductivity data")
            raw_conductivity_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                      "raw_conductivity.csv")
            raw_conductivity_total.to_csv(raw_conductivity_directory, index=False)
            self.parameters_dict['raw_data']['conductivity'] = raw_conductivity_directory

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} -  processing failed")

        self.save_parameters_dict()


class ProcessSpectroscopy(TaskTemplate):
    """
    This task processes the spectroscopy data by position. To calculate the height of the gaussian for a sample, we must
    process 4 files: blank reflectance, blank transmission, reflectance, and transmission.
    Blank reflectance and transmission are only found in the first sample for a given campaign. As such, if we are only
    processing a single campaign, we must back track through the samples to find the blank reflectance and transmission
    data.
    """

    def requires(self):
        return CampaignParameters()

    def run(self):

        self.get_parameters_dict()
        LoggingConfig.logger.info("running ProcessSpectroscopy")
        LoggingConfig.logger.info(f"{self._get_classname()} - Processing Spectroscopy")

        processed_spectra_list = []
        raw_spectra_list = []
        units = {}

        spectroscopy_ctrl_index = self.parameters_dict.get('spectroscopy_ctrl_index', None)

        try:
            # Create a CSV of the experiment dictionary for each sample.
            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():
                sample_number = int(sample_number)
                blank_spectra = {}

                if 'UVVIS' not in os.listdir(sample_directory):
                    raise Exception('UVVIS directory doesnt exist')
                else:
                    spectra_directory = os.path.join(sample_directory, 'UVVIS')

                blank_files_all_pos = [file for file in os.listdir(spectra_directory) if
                                       'blank' in file and file.endswith('.csv')]
                blank_files_all_pos.sort()

                # determine which files to use for calculations
                if spectroscopy_ctrl_index is not None:
                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Using control index to determine which spectroscopy file to use for calculations")
                    spectra_files_all_pos = [file for file in os.listdir(spectra_directory) if file.startswith(
                        '{:03d}'.format(spectroscopy_ctrl_index)) and 'blank' not in file]
                else:
                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - No control index given - using the default file")
                    spectra_files_all_pos = [file for file in os.listdir(spectra_directory) if 'blank' not in file]

                spectra_files_all_pos.sort()

                # get number  of unique positions
                positions = [lhf.file_name_parser(filename)[3] for filename in spectra_files_all_pos]
                positions = list(set(positions))
                positions.sort()

                for position in positions:
                    position_number = int(position)
                    blank_spectra[position_number] = {}

                    spectra_pos = [file for file in spectra_files_all_pos if
                                   lhf.file_name_parser(file)[3] == position and file.endswith('.csv')]
                    spectra_blank_pos = [file for file in blank_files_all_pos if
                                         lhf.file_name_parser(file)[3] == position]

                    if not spectra_blank_pos:
                        raise Exception('Blank slide data NOT FOUND in sample directory')
                    elif len(spectra_pos) != len(spectra_blank_pos):
                        raise ValueError('Missmatched number of blank spectra files and sample spectra files')
                    elif len(spectra_pos) != 2:
                        raise Exception(f"Must have a single reflectance file and single transmission data file.")

                    for file in spectra_blank_pos:
                        if '_t' in file:
                            blank_transmission_directory = os.path.join(spectra_directory, file)
                            blank_transmission = pd.read_csv(blank_transmission_directory)
                            blank_transmission['sample'] = sample_number
                            blank_transmission['position'] = position_number
                            blank_transmission['r-t'] = 't'
                            blank_transmission['blank'] = True

                            blank_spectra[position_number]['t'] = blank_transmission
                        else:
                            blank_reflectance_directory = os.path.join(spectra_directory, file)
                            blank_reflectance = pd.read_csv(blank_reflectance_directory)
                            blank_reflectance['sample'] = sample_number
                            blank_reflectance['position'] = position_number
                            blank_reflectance['r-t'] = 'r'
                            blank_reflectance['blank'] = True

                            blank_spectra[position_number]['r'] = blank_reflectance

                    if blank_spectra is None:
                        raise Exception(f"Missing blank reflectance or transmission data in SAMPLE {sample_number}")

                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Processing Spectroscopy for sample {sample_number}, position {position_number}")

                    for file in spectra_pos:
                        if '_t' in file:
                            transmission_directory = os.path.join(spectra_directory, file)
                            transmission = pd.read_csv(transmission_directory)
                            transmission['sample'] = sample_number
                            transmission['position'] = position_number
                            transmission['r-t'] = 't'
                            transmission['blank'] = False
                        else:
                            reflectance_directory = os.path.join(spectra_directory, file)
                            reflectance = pd.read_csv(reflectance_directory)
                            reflectance['sample'] = sample_number
                            reflectance['position'] = position_number
                            reflectance['r-t'] = 'r'
                            reflectance['blank'] = False

                    r_0 = blank_reflectance['calibrated_signal']
                    t_0 = blank_transmission['calibrated_signal']
                    r_1 = reflectance['calibrated_signal']
                    t_1 = transmission['calibrated_signal']
                    x_nm = reflectance['x_(nm)']

                    # Calculate absorbance of film
                    absorbance_df = m.calculate_film_absorbance(r_0, t_0, r_1, t_1, x_nm)

                    # calc_absorbance_from_t_and_r
                    a_data = m.calc_absorbance_from_t_and_r(t_data=transmission, r_data=reflectance)
                    a_data = a_data.rename(
                        columns={
                            'calibrated_signal': 'absorbance',
                            'calibrated_signal_smoothed': 'absorbance_smoothed'
                        })

                    # Set regions for gaussian
                    default_gaussian = [m.add_gaussian_region(name='a', upper_bound=2.5, lower_bound=2.45)]

                    # Initialize gaussian
                    default_gaussian = m.add_gaussian_to_region(gaussian_bounds_ev=default_gaussian, region_name='a',
                                                                a=1.1, b=4.2, c=0.2)

                    # Select the gaussian dictionary (list of 1)
                    default_gaussian = default_gaussian[0]

                    # Find the log absorbance within the boundaries
                    y_range_abs = absorbance_df['log_A_f'][
                        (absorbance_df['energy_(eV)'] < default_gaussian['bounds_ev'][0]) &
                        (absorbance_df['energy_(eV)'] > default_gaussian['bounds_ev'][1])]

                    height_gaussian = np.mean(y_range_abs)
                    units['height_gaussian'] = {'height_gaussian': 'unitless'}

                    spectra_dict = {
                        'sample': sample_number,
                        'position': position_number,
                        'height_gaussian': height_gaussian
                    }

                    # Append processed data to larger dataframe
                    processed_df = pd.DataFrame.from_dict(spectra_dict, orient='index')
                    transpose_processed_df = processed_df.T
                    processed_spectra_list.append(transpose_processed_df)

                    # Combine the data
                    pos_raw = pd.concat([blank_transmission, blank_reflectance, reflectance, transmission])
                    combined_raw = pd.merge(pos_raw, absorbance_df, on=['x_(nm)'])
                    combined_absorb = pd.merge(combined_raw, a_data, on=['x_(nm)'])
                    del combined_absorb['Unnamed: 0']
                    del combined_absorb['calibrated_signal_smoothed']
                    del combined_absorb['baseline_dark_smoothed']
                    del combined_absorb['baseline_light_smoothed']
                    del combined_absorb['raw_signal_smoothed']
                    del combined_absorb['absorbance_smoothed']

                    raw_spectra_list.append(combined_absorb)

            # Aggregate raw and processed data
            processed_total = pd.concat(processed_spectra_list)

            # Save the position data
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving position spectra data")
            processed_spectra_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                       "position_spectra.csv")
            processed_total.to_csv(processed_spectra_directory, index=False)
            self.parameters_dict['position_data']['spectroscopy'] = processed_spectra_directory

            # Save the position data
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving sample spectra data")
            processed_sample_spectra_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                              "sample_spectra.csv")
            sample_df = processed_total.groupby(['sample']).mean()
            sample_df.reset_index(level=0, inplace=True)
            del sample_df['position']
            sample_df.to_csv(processed_sample_spectra_directory, index=False)
            self.parameters_dict['sample_data']['spectroscopy'] = processed_sample_spectra_directory

            raw_total = pd.concat(raw_spectra_list)
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving raw spectra data")
            raw_spectra_directory = os.path.join(self.parameters_dict['processed_data_directory'], "raw_spectra.csv")
            raw_total.to_csv(raw_spectra_directory, index=False)
            self.parameters_dict['raw_data']['spectroscopy'] = raw_spectra_directory
            del raw_total

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")

        self.save_parameters_dict()


class ProcessXRF(TaskTemplate):
    """
    This task analyzes Bruker M4 tornado XRF hypermaps at one or more positions for one or
    more samples. The end result is a .CSV file that is saved in the results directory.
    """

    def requires(self):
        return CampaignParameters()

    def run(self):
        """
        Process the XRF data.
        :return: None.
        """
        def transform_coords(coordinates):
            slide_center_x = 12.85  # mm
            slide_center_y = 37.9
            slide_width = 25.7
            slide_height = 75.8

            transform = [np.array([[1, 0], [0, -1]]), np.array([0, slide_height])]

            input_array = np.array([coordinates['x'], coordinates['y']])

            output_array = input_array.dot(transform[0])  # rotate coordinate system

            output_array = output_array + transform[1]  # translate coordinate system
            return {'x': output_array[0], 'y': output_array[1]}

        LoggingConfig.logger.info("running ProcessXRF")
        LoggingConfig.logger.info(f"{self._get_classname()} - Processing XRF")

        # Load the global parameters dict
        self.get_parameters_dict()

        # Create a place to iteratively store the XRF data for each sample.
        poi_data = []

        # Get the origin of the XRF
        with open(self.parameters_dict['config_directory']) as f:
            config_dict = json.load(f)
            origin_x = config_dict['XRF']['XRF_ORIGIN_X']
            origin_y = config_dict['XRF']['XRF_ORIGIN_Y']

            # Element of interest (default to Pd)
            if config_dict['XRF']['ELEMENT'] is not None:
                if type(config_dict['XRF']['ELEMENT']) is not list:
                    elements = [config_dict['XRF']['ELEMENT']]
                else:
                    elements = config_dict['XRF']['ELEMENT']
            else:
                elements = ['pd']

        # For each sample number, path:
        for sample_number, sample_path in \
                self.parameters_dict['sample_directories'].items():

            # Work with the first bcf file found. This will break for multiple BCF files.
            xrf_path = f'{sample_path}/XRF'
            for file in os.listdir(xrf_path):
                if file.endswith('.bcf'):
                    break

            # Make HyperSample object
            h = HyperPos(
                f'{xrf_path}/{file}',
                sid=sample_number,
                origin_x=origin_x,
                origin_y=origin_y,
            )

            # Save graphics of positions and samples.
            map_dir = f'{self.parameters_dict["processed_data_directory"]}/xrf_maps'
            os.makedirs(map_dir, exist_ok=True)

            for element in elements:

                # save element data as csv
                element_data = h.get_element_int(element=element)
                element_data_path = f'{map_dir}/{self.parameters_dict["campaign_name"]}_s{int(sample_number):03}_{element}_'
                np.savetxt(f'{element_data_path}map.csv',element_data,delimiter=",")

                # Save map
                map_path = f'{map_dir}/{self.parameters_dict["campaign_name"]}_s{int(sample_number):03}_{element}_'
                h.make_element_map(element=element, path=f'{map_path}map.png')

                # Get each POI, and record
                # Load in the positions of the conductance data
                with open(f'{self.parameters_dict["campaign_directory"]}/config.json', 'rb') as f:
                    old_p = json.load(f)['POINTS_OF_INTEREST'] # untransformed
                    trans_p = [transform_coords(trans) for trans in old_p]
                    p = pd.DataFrame(trans_p)

                # For each position, calculate XRF spot
                for i in p.index.to_list():
                    a = h.get_physical_area(
                        center_x=p.iloc[i]['x'],
                        center_y=p.iloc[i]['y'],
                        x_span=3,
                        y_span=3,
                        element=element,
                    )
                    poi_data.append(dict(
                        sample=sample_number,
                        position=i,
                        xrf_avg=a.mean(),
                        xrf_std=a.std(),
                    ))

            # Pickle out the hypos class
            # h_dir = f'{self.parameters_dict["processed_data_directory"]}/xrf_hyper'
            # os.makedirs(h_dir, exist_ok=True)
            # pickle.dump(
            #     h,
            #     open(
            #         f'{h_dir}/{self.parameters_dict["campaign_name"]}_s{int(sample_number):03}.pkl', 'wb'),
            # )

            # Generate and store XRF summary data for that sample.
            # frame = h.generate_sample_frame([element])

        # Create dict, sort, and save to file
        # params = pd.concat(frames)
        # params.sort_values(by=['sample', 'position'], inplace=True)
        poi_data = pd.DataFrame(poi_data)

        path = f'{self.parameters_dict["processed_data_directory"]}/position_xrf.csv'
        poi_data.to_csv(
            path,
            sep=',',
            index=False,
        )

        # Store the path to the XRF data in the parameters dict
        self.parameters_dict['position_data']['xrf'] = path

        # Save the changes made to the paramters dict.
        self.save_parameters_dict()


class ProcessElectrolyticTestCellData(TaskTemplate):
    """
    This task calculates stability from the test_cell using an integration of voltage as a proxy. Lower values are more stable
    """

    def requires(self):
        return CampaignParameters()

    def run(self):
        """
        Process the IV and mass spec data.
        :return: None.
        """
        LoggingConfig.logger.info("running ProcessElectrolyticStability")
        LoggingConfig.logger.info(f"{self._get_classname()} - Processing electrolytic stability")

        # Load the global parameters dict
        self.get_parameters_dict()
        try:
            # For each sample number, path:
            stability_data = []
            for sample_number, sample_path in self.parameters_dict['sample_directories'].items():

                iv_file_name = os.path.join(sample_path, "electrolyzer", "ivium.csv")

                df = pd.read_csv(iv_file_name)

                start_time = df['sec_from_start'].iloc[0]
                df['base_time'] = df['sec_from_start'] - start_time

                non_zero = df[df["setpoint_current(A)"] != 0]

                non_zero["v_normalized_i"] = non_zero["output_voltage(V)"] / non_zero["setpoint_current(A)"]

                y = non_zero["v_normalized_i"]
                x = non_zero["base_time"]

                integration = integrate.trapz(y, x=x)
                total_time = non_zero["base_time"].iloc[-1] - non_zero["base_time"].iloc[0]
                integration_normalize = integration / total_time

                # Fe calc
                fe_filename = os.path.join(sample_path, "electrolyzer", "fe.json")
                with open(fe_filename, "r") as fe_file:
                    fe_dict = json.load(fe_file)

                # fe_list = fe_dict["fe"][1:-1].split(" ")
                # fe_num_list = [float(num) for num in fe_list]
                # fe =fe_num_list[0]  # use first value in list as Fe
                feco = fe_dict["feCO"][0]
                feh2 = fe_dict["feH2"][0]

                data_dict = {"sample": sample_number, "electrolytic_stability": integration_normalize, "feCO": feco, "feH2": feh2}
                stability_data.append(data_dict)
            data_df = pd.DataFrame(stability_data, index=[0])
            data_save_path = os.path.join(self.parameters_dict['processed_data_directory'], "sample_electrolytic_stability.csv")
            data_df.to_csv(data_save_path, index=False)
            self.parameters_dict['sample_data']['electrolytic_stability'] = data_save_path
            self.save_parameters_dict()
        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")


class ProcessContactAngle(TaskTemplate):
    """
    This task calculates contact angle. Note that the actual calculation is done at the contact angle station
    """

    def requires(self):
        return CampaignParameters()

    def run(self):
        """
        Process the contact angle data.
        :return: None.
        """
        LoggingConfig.logger.info("running ProcessContactAngle")
        LoggingConfig.logger.info(f"{self._get_classname()} - Processing Contact Angle")

        # Load the global parameters dict
        self.get_parameters_dict()
        try:
            # For each sample number, path:
            contact_angle_data = []
            for sample_number, sample_path in self.parameters_dict['sample_directories'].items():

                ca_file_name = os.path.join(sample_path, "contact_angle", "contact_angle.csv")

                df = pd.read_csv(ca_file_name)

                ca_non_zero = df[df["Contact Angle - Right [deg]"] != 0]

                ca_mean = ca_non_zero["Contact Angle - Right [deg]"].mean()
                ca_std = ca_non_zero["Contact Angle - Right [deg]"].std()
                ca_mean = 0 if pd.isna(ca_mean) else ca_mean
                ca_std = 0 if pd.isnull(ca_std) else ca_std

                data_dict = {"sample": sample_number, "contact_angle_avg": ca_mean, "contact_angle_std": ca_std}
                contact_angle_data.append(data_dict)
            data_df = pd.DataFrame(contact_angle_data, index=[0])
            data_save_path = os.path.join(self.parameters_dict['processed_data_directory'], "sample_contact_angle.csv")
            data_df.to_csv(data_save_path, index=False)
            self.parameters_dict['sample_data']['contact_angle'] = data_save_path
            self.save_parameters_dict()
        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")


class ProcessHaze(TaskTemplate):
    """
    This task calculates haze. Note that the actual calculation is done at the haze station
    """

    def requires(self):
        return CampaignParameters()

    def run(self):
        """
        Process the haze data.
        :return: None.
        """
        LoggingConfig.logger.info("running ProcessHaze")
        LoggingConfig.logger.info(f"{self._get_classname()} - Processing Haze")

        # Load the global parameters dict
        self.get_parameters_dict()
        try:
            # For each sample number, path:
            haze_data = []
            for sample_number, sample_path in self.parameters_dict['sample_directories'].items():

                haze_file_name = os.path.join(sample_path, "haze", "spectrometer_data_avg.json")
                with open(haze_file_name) as fp:
                    haze_dict = json.load(fp)
                    haze_avg = haze_dict["measurement"]
                data_dict = {"sample": sample_number, "haze_avg": haze_avg}
                haze_data.append(data_dict)
            data_df = pd.DataFrame(haze_data, index=[0])
            data_save_path = os.path.join(self.parameters_dict['processed_data_directory'], "sample_haze.csv")
            data_df.to_csv(data_save_path, index=False)
            self.parameters_dict['sample_data']['haze'] = data_save_path
            self.save_parameters_dict()
        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")


class CalculatePseudomobility(TaskTemplate):
    """
    This task combines the spectroscopy and conductivity subpipes. Pseudomobility for a sample is calculated by getting
    the pseudomobility by position and then taking the average of the positions for the sample.
    """

    def requires(self):
        yield [
            ProcessSpectroscopy(),
            ProcessConductance()
        ]

    def run(self):

        self.get_parameters_dict()
        LoggingConfig.logger.info("running CalculatePseudomobility")
        # Get the processed spectroscopy and conducutivity dataframe directories
        processed_spectra_directory = self.parameters_dict['sample_data'].get('spectroscopy', None)
        processed_conductivity_directory = self.parameters_dict['sample_data'].get('conductivity', None)

        specta_df = None
        if processed_spectra_directory is not None:
            specta_df = pd.read_csv(processed_spectra_directory)

        conductivity_df = None
        if processed_conductivity_directory is not None:
            conductivity_df = pd.read_csv(processed_conductivity_directory)

        try:
            LoggingConfig.logger.info(f"{self._get_classname()} - Calculating pseudomobility")

            # Merge the conductance and height of the gaussian dataframes by position and sample.
            sample_pseudo_df = pd.merge(specta_df, conductivity_df, on=['sample'])

            # Calculate positional pseudomobility
            sample_pseudo_df['pseudomobility'] = sample_pseudo_df['conductance'] / sample_pseudo_df[
                'height_gaussian']

            del sample_pseudo_df['conductance']
            del sample_pseudo_df['height_gaussian']

            # Save the processed sample pseudomobility data
            sample_pseudomobility_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                           'sample_pseudomobility.csv')
            sample_pseudo_df.to_csv(sample_pseudomobility_directory, index=False)
            self.parameters_dict['sample_data']['pseudomobility'] = sample_pseudomobility_directory
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving sample pseudomobility data")

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")

        self.save_parameters_dict()


class CalculatePseudoconductivity(TaskTemplate):
    """
    Corrects the conductance value by the XRF absorption, to create a pseudoconductivity.

    If the confidence in the conductance IV curve fitting is too low, that point is removed
    from the sample mean.

    If the XRF map for an element is too inhomogeneous, that point is removed.

    Otherwise, the normalized pseudoconductivity is returned.
    """

    def requires(self):
        yield [
            ProcessXRF(),
            ProcessConductance(),
        ]

    def run(self):

        # Get the parameters dict
        self.get_parameters_dict()
        LoggingConfig.logger.info("running CalculatePseudoconductivity")
        # Load in the XRF and conductance data
        xrf_df = pd.read_csv(self.parameters_dict['position_data']['xrf'])
        con_df = pd.read_csv(self.parameters_dict['position_data']['conductivity'])

        # Combine the data on sid and pid.
        df = pd.merge(xrf_df, con_df, on=['sample', 'position'])

        # Sort values
        df.sort_values(by=['sample', 'position'], ignore_index=True, inplace=True)

        # Rename cond col
        df.rename(columns={'conductance': 'cond'}, inplace=True)

        # Identify zeros in conductance
        df['cond_nan'] = df['cond'].replace(0, np.nan)

        # Create the cond filter
        df = self.apply_kde_filter(
            d=df,
            col='cond_nan',
            bandwidth=0.005,
            threshold=0.3,
        )

        # Use the cond filter to mark the outliers as nans in the XRF data so that they are
        # not used in the subsequent KDE filtering.
        df['xrf_avg_rel'] = np.where(df['cond_nan_pass'], df['xrf_avg'], np.nan)

        # Create thee filter for the relevant XRF data
        df = self.apply_kde_filter(
            d=df,
            col='xrf_avg_rel',
            bandwidth=5E4,
            threshold=0.3,
        )

        # Calculate XRF-normalized conductance - changing over to use mx+b equation from earlier calibration run (2022-03-03)
        slope = 3.658E-4  # units of nm / cps
        intercept = -2.725E1  # nm

        df["thickness"] = df["xrf_avg"]*slope + intercept

        df["sheet_conductance"] = df['cond_nan'] * np.log(2) / np.pi
        df["sheet_resistance"] = 1 / df["sheet_conductance"]

        df["conductivity"] = (df["sheet_conductance"] / df["thickness"]) * 1E9
        df["resistivity"] = 1 / df["conductivity"]

        df['condxrf'] = df['cond_nan'] / df['xrf_avg']  # old objective

        # Create the XRF filter that is the union of the con and xrf filters
        df['condxrf_pass'] = df['cond_nan_pass'].values & df['xrf_avg_rel_pass'].values

        # Set zeros in condxrf to nan TODO Needed?
        # df['condxrf'][df['condxrf'] == 0] = np.nan

        # Save to file
        path = f'{self.parameters_dict["processed_data_directory"]}/position_condxrf.csv'
        df.to_csv(path, index=False, sep=',')

        # Store path location
        self.parameters_dict['position_data']['condxrf'] = path

        # Generate sample-level data
        dfs = df[['sample', 'condxrf', 'thickness', 'sheet_conductance', 'sheet_resistance', 'conductivity', 'resistivity', 'condxrf_pass']]

        for field in ['condxrf', 'thickness', 'sheet_conductance', 'sheet_resistance', 'conductivity', 'resistivity']:
            dfs[f"{field}_filtered"] = dfs[field]
            dfs[f"{field}_filtered"][~np.array(dfs['condxrf_pass'].values, dtype=bool)] = np.nan
        dfs = dfs[['sample', 'condxrf_filtered', 'thickness_filtered', 'sheet_conductance_filtered', 'sheet_resistance_filtered', 'conductivity_filtered', 'resistivity_filtered']]

        # Make groups
        ds = pd.DataFrame()
        groups = {}
        for field in ['condxrf', 'thickness', 'sheet_conductance', 'sheet_resistance', 'conductivity', 'resistivity']:
            groups[f"{field}_filtered"] = (dfs.groupby('sample')[f"{field}_filtered"])

        # Generate the std and avg of the fields
        # create a list of dfs to concat and flatten the list before concatting
        concat_list = [j for sub in [[group.mean().rename(f'{key.replace("_filtered", "")}_avg'), group.std().rename(f'{key.replace("_filtered", "")}_std'),
                                      group.count().rename(f'{key.replace("_filtered", "")}_count')] for key, group in groups.items()] for j in sub]
        ds = pd.concat(concat_list, axis=1)

        # Rewrite samples with poor counts
        for field in ['condxrf', 'thickness', 'sheet_conductance', 'sheet_resistance', 'conductivity', 'resistivity']:
            ds[f'{field}_manual'] = np.isnan(ds[f'{field}_std']) | (ds[f'{field}_count'] < 3)
            ds[f'{field}man_avg'] = ds[f'{field}_avg']
            ds[f'{field}man_std'] = ds[f'{field}_std']

            # Rewrite low pop samples
            ds.loc[ds[f'{field}_manual'], f'{field}man_avg'] = 0
            ds.loc[ds[f'{field}_manual'], f'{field}man_std'] = 1E-7

        # Path to store sample-level data
        path = f'{self.parameters_dict["processed_data_directory"]}/sample_condxrf.csv'

        # Save sample-level data
        ds.to_csv(path, sep=',')
        self.parameters_dict['sample_data']['condxrf'] = path

        # Save parameters dict
        self.save_parameters_dict()

    def apply_kde_filter(
            self,
            d: pd.DataFrame,
            col: str,
            bandwidth: float,
            threshold: float,
    ):

        # The robust mean through KDE has been written as a function so it can be applied to
        # both XRF and cond separately.

        # Create a place to store the bandwidth and threshold values
        if self.parameters_dict.get('filters') is None:
            self.parameters_dict['filters'] = dict()

        # Create a bool series to hold the created filter result
        d[f'{col}_pass'] = False

        # Set the indices to sid, pid so that they can be merged later on
        dm = d.set_index(['sample', 'position'], inplace=False)

        # Iterate through each sample
        for s in d['sample'].unique():

            # Setup and store the filtering variables. These are being stored so
            # plotting tools can use it.
            self.parameters_dict['filters'][f'{col}_bandwidth'] = bandwidth
            self.parameters_dict['filters'][f'{col}_threshold'] = threshold

            # Select the data for training the kernel
            ds = d[d['sample'] == s]

            # If there are no points that are not nan, return a defined responsee
            if ~np.any(~np.isnan(ds[col])):

                du = pd.DataFrame(
                    {
                        'sample': s,
                        'position': ds[~np.isnan(ds[col])]['position'],
                        f'{col}_pass': False
                    }
                )

            # If there are samples to filter:
            else:

                # Remove nans (for cond values)
                x = np.vstack(ds[col][~np.isnan(ds[col])])

                # Create kernel and fit data
                kde = KernelDensity(
                    bandwidth=bandwidth,
                    kernel='gaussian',
                )
                kde.fit(x)

                # Create a large sampling of the array such that KDE such that it can be
                # normalized correctly
                res = 1000
                x_min, x_max = x.min(), x.max()
                x_range = x_max - x_min
                p_min, p_max = x_min - 0.1 * x_range, x_max + 0.1 * x_range
                norm_range = np.vstack(np.linspace(p_min, p_max, res))

                # Fit both the samples and the high density sampling
                x_pred = np.exp(kde.score_samples(x))
                x_hden = np.exp(kde.score_samples(np.concatenate([x, norm_range], axis=0)))

                # Normalize the sampled data against the high density sampling
                x_pred = x_pred / max(x_hden)

                # Test each point to see if it passes the threshold
                x_pred_filter = x_pred > threshold

                # Create a dataframe to update the master dataframe with
                du = pd.DataFrame(
                    {
                        'sample': s,
                        'position': ds[~np.isnan(ds[col])]['position'],
                        f'{col}_pass': x_pred_filter
                    }
                )

            # Set the indecies to sid, pid so that they can be merged
            du.set_index(['sample', 'position'], inplace=True)

            # Update the dataframe
            dm.update(du)

        # Once all samples are completed, reset the index and return
        dm.reset_index(inplace=True)
        return dm


class ProcessProfilometry(TaskTemplate):
    """
    This task processes the spectroscopy data by position. To calculate the height of the gaussian for a sample, we must
    process 4 files: blank reflectance, blank transmission, reflectance, and transmission.
    Blank reflectance and transmission are only found in the first sample for a given campaign. As such, if we are only
    processing a single campaign, we must back track through the samples to find the blank reflectance and transmission
    data.
    """

    def requires(self):
        return CampaignParameters()

    def run(self):

        self.get_parameters_dict()
        LoggingConfig.logger.info("running ProcessProfilometry")
        try:
            LoggingConfig.logger.info(f"{self._get_classname()} - Processing Profilometery data")

            position_data = []

            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():
                sample_number = int(sample_number)

                if 'PROFILOMETRY' not in os.listdir(sample_directory):
                    continue

                else:
                    profilometry_directory = os.path.join(sample_directory, 'PROFILOMETRY')
                    profilometry_files = [file for file in os.listdir(profilometry_directory) if file.endswith('.OPDx')]

                    for profilometry_file in profilometry_files:
                        filename = profilometry_file.split('.')[0]
                        position_number = int(filename.split('pos_')[1])

                        filepath = os.path.join(profilometry_directory, profilometry_file)

                        raw_profile = profilometry.convert_profile_binary_to_csv(filepath)
                        leveled_profile = profilometry.level_profile(raw_profile, filepath, plot=False)
                        leveled_profile['film_height_level'] = leveled_profile['height_level'] * leveled_profile[
                            'filtered_film_map']

                        film_df = leveled_profile[leveled_profile['film_height_level'] > 0]

                        standard_dev = np.std(film_df['film_height_level'])
                        variance = np.var(film_df['film_height_level'])
                        mean = np.mean(film_df['film_height_level'])
                        median = np.median(film_df['film_height_level'])

                        x = np.array(film_df['rolling_mean'])
                        peaks, _ = find_peaks(x)
                        prominences = peak_prominences(x, peaks)[0]

                        prominent_peaks = peaks[prominences > 0.5 * standard_dev]
                        peaks_over_interval = len(peaks) / len(film_df['film_height_level'])
                        prominent_peaks_over_interval = len(prominent_peaks) / len(film_df['film_height_level'])

                        # plt.plot(x)
                        # plt.plot(peaks, x[peaks], "x")
                        # plt.show()

                        prof_dict = {
                            'sample': sample_number,
                            'position': position_number,
                            'thickness_standard_deviation': standard_dev,
                            'thickness_variance': variance,
                            'thickness_mean': mean,
                            'thickness_median': median,
                            'total_peaks': len(peaks),
                            'prominent_peaks': len(prominent_peaks),
                            'peaks_over_interval': peaks_over_interval,
                            'prominent_peaks_over_interval': prominent_peaks_over_interval
                        }

                        position_data.append(prof_dict)

            # Aggregate raw and processed data
            processed_total = pd.DataFrame(position_data)

            # Save the position data
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving position profilometer data")
            processed_profilometry_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                            "position_profilometry.csv")
            processed_total.to_csv(processed_profilometry_directory, index=False)
            self.parameters_dict['position_data']['profilometry'] = processed_profilometry_directory

            # Save the sample data
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving sample profilometer data")
            processed_sample_profilometry_directory = os.path.join(
                self.parameters_dict['processed_data_directory'],
                "sample_profilometry.csv")
            sample_df = processed_total.groupby(['sample']).mean()
            sample_df.reset_index(level=0, inplace=True)
            del sample_df['position']
            sample_df.to_csv(processed_sample_profilometry_directory, index=False)
            self.parameters_dict['sample_data']['profilometry'] = processed_sample_profilometry_directory

        except:

            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")

        self.save_parameters_dict()


class ProcessInputs(TaskTemplate):

    def requires(self):
        return CampaignParameters()

    def run(self):
        """
        Process all sample inputs into a sample csv.
        :return: None.
        """
        LoggingConfig.logger.info("running ProcessInputs")

        LoggingConfig.logger.info(f"{self._get_classname()} - Processing inputs")

        # Load the global parameters dict
        self.get_parameters_dict()

        # Load in the opt parameters.
        path = f'{self.parameters_dict["campaign_directory"]}/opt_state.json'

        # Only run if opt_state.json exists
        if os.path.exists(path):
            # Load
            opt = json.load(open(path))
            opt['results'].pop('y', None)

            # Made dataframe
            d = pd.DataFrame(opt['results'])
            d = d.add_prefix('INPUT_')

            # Add sample col
            d['sample'] = d.index

            # Save
            dpath = f'{self.parameters_dict["processed_data_directory"]}/sample_temp.csv'
            d.to_csv(dpath, index=False)

            # Store path for later merging
            self.parameters_dict['sample_data']['sample_temp'] = dpath

            # Save params dict
            self.save_parameters_dict()


class ProcessSampleParameters(TaskTemplate):
    """
    Get the sample parameters json in csv format for aggregation
    """

    def requires(self):
        return CampaignParameters()

    def run(self):
        self.get_parameters_dict()
        LoggingConfig.logger.info("running ProcessSampleParameters")
        try:
            LoggingConfig.logger.info(f"{self._get_classname()} - Reformatting Sample Parameters")

            df_dicts = []

            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():
                sample_number = int(sample_number)

                if 'sample_parameters.json' in os.listdir(sample_directory):
                    with open(os.path.join(sample_directory, 'sample_parameters.json')) as json_file:
                        sample_paramers_dict = json.load(json_file)
                    realized = sample_paramers_dict.get('realized', {})
                    requested = sample_paramers_dict.get('requested', {})

                    robot_realized = sample_paramers_dict.get('robot_realized', {})
                    robot_requested = sample_paramers_dict.get('robot_requested', {})

                    df_dict = {}

                    for k, v in realized.items():
                        df_dict[f"{k}_realized"] = v

                    for k, v in requested.items():
                        df_dict[f"{k}_requested"] = v

                    for k, v in robot_realized.items():
                        df_dict[f"{k}_robot_realized"] = v

                    for k, v in robot_requested.items():
                        df_dict[f"{k}_robot_requested"] = v

                    df_dict['sample'] = sample_number
                    df_dicts.append(df_dict)

            df = pd.DataFrame(df_dicts)

            # Save the processed sample parameters
            sample_parameters_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                       'sample_parameters.csv')
            df.to_csv(sample_parameters_directory, index=False)
            self.parameters_dict['sample_data']['sample_parameters'] = sample_parameters_directory
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving sample parameters data")

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")

        self.save_parameters_dict()


class AggregateData(TaskTemplate):
    """
    Aggregate position and sample data.
    """

    def requires(self):
        yield [
            # ProcessImages(),
            # ProcessSampleParameters(),
            # CalculatePseudoconductivity()
        ]

    def run(self):

        self.get_parameters_dict()
        LoggingConfig.logger.info("running AggregateData")
        # Gather all the sample-level frames that have been created by the pipeline.
        sample_df_list = []
        for sample_directory in self.parameters_dict['sample_data'].values():
            try:
                sample_df = pd.read_csv(sample_directory)
                sample_df_list.append(sample_df)
            except pd.errors.EmptyDataError:
                pass

        # Combine all of these dataframes into one dataframe.
        combined_df = reduce(lambda left, right: pd.merge(left, right, on='sample'), sample_df_list)

        # Replace nans with 0.
        combined_df.replace(np.nan, 0, inplace=True)

        # Get the path for this dataframe to be saved to.
        LoggingConfig.logger.info(f"{self._get_classname()} - Saving aggregate sample data")
        aggregate_sample_directory = os.path.join(
            self.parameters_dict['processed_data_directory'],
            'sample_aggregate.csv'
        )

        # Save this dataframe to disk. The optimizer will use a column from this frame
        # as the cost value.
        combined_df.to_csv(aggregate_sample_directory, index=False)
        self.parameters_dict['aggregate_data']['sample'] = aggregate_sample_directory

        # Save parameter dict
        self.save_parameters_dict()


class CustomTransformations(TaskTemplate):

    def requires(self):
        return [
            AggregateData(),
        ]

    def run(self):

        self.get_parameters_dict()
        LoggingConfig.logger.info("running CustomTransformations")
        # Generate a dataframe with the transformed variables with cost
        cost_parameters_dict = self.parameters_dict.get('cost_parameters', None)
        if cost_parameters_dict is None:
            LoggingConfig.logger.info("No cost parameters provided. Cost not calculated.")

        else:
            sample_cost_directory = os.path.join(self.parameters_dict['processed_data_directory'], 'sample_cost.csv')
            self.parameters_dict['sample_cost'] = sample_cost_directory
            cost_df = cfb.CostFunction(processing_parameters=self.parameters_dict,
                                       cost_parameters=cost_parameters_dict).cost()
            cost_df.to_csv(sample_cost_directory, index=False)

        self.save_parameters_dict()


class DataEntry(TaskTemplate):
    """
    Data is entered at the last task in the pipeline.
    """

    def requires(self):
        # passes the dir dictionary up to the preceding task.
        return CustomTransformations()

    def run(self):
        self.get_parameters_dict()
        LoggingConfig.logger.info("running DataEntry")
        LoggingConfig.logger.info(f"{self._get_classname()} - Complete the pipeline")

        self.save_parameters_dict()

if __name__ == '__main__':
    xrf_path = 'a'
    file = 'b'
    element = 'pd'

    h = HyperPos(
        f'{xrf_path}/{file}',
        sid=sample_number,
        origin_x=origin_x,
        origin_y=origin_y,
    )

    # save element data as csv
    element_data = h.get_element_int(element=element)
    element_data_path = f'{map_dir}/{self.parameters_dict["campaign_name"]}_s{int(sample_number):03}_{element}_'
    np.savetxt(f'{element_data_path}map.csv', element_data, delimiter=",")