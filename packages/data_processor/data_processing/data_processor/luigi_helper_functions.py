import os
import pandas as pd
import collections
import six
import shutil
import time
from datetime import datetime

# python 3.8+ compatibility
try:
    collectionsAbc = collections.abc
except:
    collectionsAbc = collections

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


def find_blank_spectroscopy(parameters_dict, sample_number):
    """
    This function searches the sample folders to find a the sample that contains the blank spectra data.
    :param parameters_dict:
    :param sample_number:
    :return:
    """
    campaign_files = os.listdir(parameters_dict['campaign_directory'])

    sample_folders = [folder for folder in campaign_files if
                      folder.startswith('sample_') and int(folder.split('_')[1]) <= sample_number]

    # Start with the newest spectra and search upward.
    sample_folders.sort(reverse=True)

    for sample in sample_folders:
        sample_folder = os.path.join(parameters_dict['campaign_directory'], sample)
        sample_folder_files = os.listdir(sample_folder)
        sample_number = int(sample.split("_")[1])

        blank_spectra = {}

        position_folders = [folder for folder in sample_folder_files if folder.startswith('pos_')]

        for position in position_folders:
            position_folder_directory = os.path.join(sample_folder, position)
            position_folder_files = os.listdir(position_folder_directory)
            position_number = int(position.split("_")[1])

            blank_files = [file for file in position_folder_files if
                           'blank' in file and file.endswith('.csv') and file.startswith('_uvvis')]

            if len(blank_files) != 2:
                break

            blank_spectra[position_number] = {}

            for file in blank_files:
                if 't-data' in file:
                    blank_transmission_directory = os.path.join(position_folder_directory, file)
                    blank_transmission = pd.read_csv(blank_transmission_directory)

                else:
                    blank_reflectance_directory = os.path.join(position_folder_directory, file)
                    blank_reflectance = pd.read_csv(blank_reflectance_directory)

            blank_spectra[position_number]['r'] = blank_reflectance
            blank_spectra[position_number]['t'] = blank_transmission

        if len(blank_spectra) > 0:
            return blank_spectra

    return None


def update(d, u):
    """
    Nested dictionary update function
    :param d:
    :param u:
    :return:
    """
    for k, v in six.iteritems(u):
        dv = d.get(k, {})
        if not isinstance(dv, collectionsAbc.Mapping):
            d[k] = v
        elif isinstance(v, collectionsAbc.Mapping):
            d[k] = update(dv, v)
        else:
            d[k] = v
    return d


def log_find_copy(src_dir, dest_dir, sample_num, log_base_name='campaign_log.log'):
    """
    Find a specific log file that is relevant to the sample number
    provided and copies it into destination directory. Requires the log names to
    be in the following format - 'log_base_name.sample_num'
    :param src_dir:
    :param dest_dir:
    :param sample_num:
    :param log_base_name:
    :return:
    """
    log_files = [log for log in os.listdir(src_dir) if log_base_name in log]

    for log in log_files:
        if log == log_base_name:
            log_num = 0
        else:
            log_num = int(log[len(log_base_name) + 1:])

        if log_num == sample_num:
            log_des_path = os.path.join(dest_dir, 'sample_log.log')
            shutil.copyfile(os.path.join(src_dir, log), log_des_path)


def consume_sensor_data(csv_path, sample, component, df_columns=['date_time', 'time_stamp_abs', 'sensor_reading']):
    df_tmp = pd.read_csv(csv_path)
    df_tmp.columns = df_columns

    # add sample number to dataframe
    df_tmp['sample'] = [sample] * len(df_tmp['sensor_reading'])

    # add component that data came from to df
    df_tmp['component'] = [component] * len(df_tmp['sensor_reading'])

    # get the relative time
    df_tmp['time_stamp_rel'] = df_tmp['time_stamp_abs'] - df_tmp['time_stamp_abs'].min()

    return df_tmp


def process_log_into_df(log_path, must_inc=('INFO', 'DEBUG'),
                        ignore=('speed_manager', 'ada_processing', 'ada_toolbox', 'Timer',
                                'CONSOLE_LOGGER_LEVEL', 'Debug mode active', 'Iteration'), ):
    """
    This function filters a campaign log for useful information and converts it intoa data frame
    :param log_path:
    :param must_inc:
    :param ignore:
    :return:
    """

    filt_lines = []
    # filter a log based on must_inc variable
    with open(log_path, 'r', encoding='utf-8', errors='ignore') as fr:
        for line in fr:
            contains = False
            for key in must_inc:
                contains = contains or (key in line)
            for key in ignore:
                contains = contains and not (key in line)
            if contains:
                filt_lines.append(line)

    # parse lines with ': ' delimiter
    rows = list(map(lambda x: x.split(': '), filt_lines))
    # take only the first 4 columns
    rows = list(map(lambda x: x[0:5], rows))

    # delete the logger level tag
    for row in rows:
        if len(row) < 4:
            del row
        else:
            del row[3]

    # construct dataframe
    columns = ['date_time', 'caller', 'function', 'action']
    df = pd.DataFrame(rows, columns=columns)

    # convert time
    time_format = '%Y-%m-%d %H:%M:%S,%f'

    df['date_time'] = df['date_time'].apply(lambda t: datetime.strptime(t, time_format))
    df['time_stamp_abs'] = df['date_time'].apply(lambda t: time.mktime(t.timetuple()) + t.microsecond / 1E6)

    return df


def sync_df(sensor_df, log_df, sync_interval=0.5):
    # get synced log
    for index, row in log_df.iterrows():
        ix = (row['time_stamp_abs'] <= sensor_df['time_stamp_abs'] + sync_interval)
        sensor_df.loc[ix, 'action'] = row['action']


def file_name_parser(filename: str):

    if filename.startswith('.') or filename.endswith('.ini'):
        return

    name = filename.split('.')[0]

    # if filename with no  extension was passed
    try:
        extension = filename.split('.')[1]
    except IndexError:
        extension = None

    # the current filename standard is <control_index>-<equipment>-<other_notes>-<pos_N>
    # the file name should always have control_index and equipment
    array = name.split('-')

    if len(array) == 3 and 'pos' in array[-1]:
        ctrl_ix = array[0]
        equip = array[1]
        other = None
        position = array[2].split('_')[-1]

    elif len(array) == 3:
        ctrl_ix = array[0]
        equip = array[1]
        other = array[2]
        position = None

    elif len(array) == 4:
        ctrl_ix = array[0]
        equip = array[1]
        other = array[2]
        position = array[3].split('_')[-1]

    elif len(array) == 5:
        ctrl_ix = array[0]
        equip = array[1]
        other = array[2]
        position = array[3].split('_')[1]

    else:
        raise ValueError(f'Error parsing the filename {filename}')

    return ctrl_ix, equip, other, position