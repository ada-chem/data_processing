import logging
from packages.data_processor.data_processing.data_processor.data_processor import DataProcessing
import packages.data_processor.data_processing.data_processor.cost_function_builder as cost_function_builder
import argparse
import sys


parser = argparse.ArgumentParser(description='Process campaign data and return the cost value')
parser.add_argument('-p', '--pipeline_name', required=True, type=str, help='pipeline to use for processing')
parser.add_argument('-d', '--campaign_dir', required=True, type=str, help='campaign folder to process, e.g. /path/to/campaign')
parser.add_argument('-r', '--results_dir', required=True, type=str, help='campaign folder to process, e.g. /path/to/save/campaign')
parser.add_argument('-s', '--sample_num', required=True, type=int, help='The sample number to process, e.g 1, 2, 14')
parser.add_argument('-v', '--value', required=True, type=str, help='The cost value to return')
parser.add_argument('-i', '--image', required=True, type=int, help='Image control index to use for scoring')
parser.add_argument('-u', '--uvvis', required=False, type=int, default=None, help='UVVIS control index to use for calculations')
parser.add_argument('-c', '--conduct', required=False, type=int, default=None, help='Conductivity control index to use for calculations')
parser.add_argument('--response', type=str, default='linear', help='Optional the response value to scale the cost function')

logger = logging.getLogger(__name__)


def run(pipeline_name,
        campaign_directory,
        results_directory,
        sample_number,
        value,
        response='linear',
        image_ctrl_index=None,
        conductivity_ctrl_index=None,
        spectroscopy_ctrl_index=None):

    cost_params = cost_function_builder.CostParameters(
        parameter={
            'response': response,
            'value': value
        },
    )

    dp = DataProcessing(
        pipeline_name=pipeline_name,
        campaign_directory=campaign_directory,
        results_directory=results_directory,
        cost_parameters=cost_params.dictionary(),
        sample=[sample_number],
        image_ctrl_index=image_ctrl_index,
        conductivity_ctrl_index=conductivity_ctrl_index,
        spectroscopy_ctrl_index=spectroscopy_ctrl_index
    )

    return dp.get_cost()


if __name__ == "__main__":
    # USAGE
    # # parses the cost value from the stderr output
    # sub = subprocess.run([python, script] + script_args, stderr=subprocess.PIPE, stdout=subprocess.PIPE, )
    # cost = float(re.search("COST>>>(.*)<<<", str(sub.stderr)).group(1))

    # do not change edit this block, required for robots
    args = parser.parse_args()

    cost = run(
        args.pipeline_name,
        args.campaign_dir,
        args.results_dir,
        args.sample_num,
        args.value,
        args.response,
        args.image,
        args.uvvis,
        args.conduct)

    sys.stderr.write(f"COST>>>{format(cost, '.15f')}<<<")

