import logging
import os
import pickle
from ada_tools.tcpip_template.client import Comms


logging.basicConfig(level="INFO")

logger = logging.getLogger(__name__)

SERVER_ADDRESS = (os.environ['CHRONOS'], 1055)  # address and port of xrf server


# encoding/decoding functions
def serialise(obj):
    return pickle.dumps(obj)


def deserialise(b):
    return pickle.loads(b)


class DataProcessorClient(Comms):
    def __init__(self, server_address=None, timeout=3600):
        # create logger
        self.__dict__["logger"] = logger.getChild(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)
        logger.info(f"Start DataProcessorClient")
        super().__init__(server_address=server_address, timeout=timeout)


if __name__ == '__main__':
    server = DataProcessorClient(server_address=SERVER_ADDRESS)
