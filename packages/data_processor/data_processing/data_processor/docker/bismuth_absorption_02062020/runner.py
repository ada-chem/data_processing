#!/usr/bin/python

from data_processing.data_processor.data_processor import DataProcessing
import json

json_file = '/processing_parameters_directory/processing_parameters.json'

with open(json_file) as json_file:
    data = json.load(json_file)

# Rebuild the data processing pipeline in the Docker image with VM relative directories
pipeline_name = data['pipeline_name']
cost_parameters = data['cost_parameters']
sample = data['sample']
workers = data['workers']

# Take out preconfigured directories from the data so that kwargs can be passed through to the VM
data_processing_list = [
    'campaign_directory',
    'results_directory',
    'cost_parameters',
    'sample',
    'workers',
    'pipeline_name',
    'campaign_results_directory',
    'processing_parameters_directory',
    'processed_data_directory',
    'docker',
    'docker_cache'
]

for data_key in data_processing_list:
    try:
        data.pop(data_key)
    except Exception as e:
        print(e)

# Process the data
dp = DataProcessing(
    pipeline_name=pipeline_name,
    campaign_directory="/data_pipeline",
    results_directory="/results_directory",
    cost_parameters=cost_parameters,
    sample=sample,
    workers=workers,
    docker=False,
    docker_cache=False,
    **data
)
