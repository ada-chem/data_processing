import luigi
from luigi.mock import MockTarget
import json
import os
import logging
import sys
import data_processing.data_processor.luigi_helper_functions as lhf


class LoggingConfig:
    """
    Defines the logger instance that will be used in all tasks.
    """
    logger = None

    def __init__(self, campaign_results_directory):
        # Setup logging for data processing
        self.data_processing_log_directory = os.path.join(campaign_results_directory, 'data_processing.log')
        if os.path.exists(self.data_processing_log_directory):
            os.remove(self.data_processing_log_directory)

        self.custom_logging(campaign_results_directory=campaign_results_directory)

    def custom_logging(self, campaign_results_directory, level=logging.DEBUG):
        """
        Define the logging parameters for the mutable class.

        :param campaign_results_directory: Where the logging file lives
        :param level: What level of logging to capture
        :return:
        """
        campaign_name = os.path.basename(campaign_results_directory)

        logger = logging.getLogger(campaign_name)
        logger.setLevel(level)

        logger.handlers = []

        format_string = ('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        log_format = logging.Formatter(format_string)

        # Creating and adding the console handler
        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setFormatter(log_format)
        logger.addHandler(console_handler)

        # Creating and adding the file handler
        file_handler = logging.FileHandler(self.data_processing_log_directory)
        file_handler.setFormatter(log_format)
        logger.addHandler(file_handler)

        LoggingConfig.logger = logger


class Parameters:
    """
    This class acts as a mutable super instance to the luigi tasks. Each luigi task has the dictionary available to it.
    """

    # Dictionary is a mutable argument. This is inherited by all tasks.
    dictionary = None

    def __init__(self, dictionary):
        Parameters.dictionary = dictionary


class TaskTemplate(luigi.Task, Parameters, LoggingConfig):
    """
    This class acts as a wrapper around the luigi task.
    """

    def output(self):
        """
        By default, each task has an output whose identity is defined by the task class name.

        :return: MockTarget class name. Mock target gets cleared every run.
        """
        return MockTarget(self._get_classname())

    @classmethod
    def _get_classname(cls):
        """
        :return: string representation of the class name
        """
        return str(cls.__name__)

    def get_parameters_dict(self):
        """
        To be called at the beginning of each task. Gets the parameter dictionary.
        :return:
        """
        directory = Parameters.dictionary['processing_parameters_directory']

        with open(directory) as json_file:
            data = json.load(json_file)

        self.parameters_dict = data

    def save_parameters_dict(self):
        """
        To be called at the end of each run. Updates the processing parameters JSON and signals the task is complete.
        :return:
        """

        # Get any new updates to the processing json and update. This method id thread-safe.
        directory = Parameters.dictionary['processing_parameters_directory']
        with open(directory) as json_file:
            new_data = json.load(json_file)

            self.parameters_dict = lhf.update(self.parameters_dict, new_data)

            # Save to the processing parameters json
            with open(self.parameters_dict['processing_parameters_directory'], 'w') as outfile:
                json.dump(self.parameters_dict, outfile, indent=4)

        # Luigi signal that the task is complete.
        self.output().open("w").close()
