import numpy as np
import pandas as pd
import logging
import json
import ast

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class CostParameters:
    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def dictionary(self):
        cost_dict = self.kwargs
        return dict(cost_dict)


class CostFunction:
    def __init__(self,
                 processing_parameters: dict,
                 cost_parameters: dict):

        self.sample_number = processing_parameters['sample']
        self.sample_df = pd.read_csv(processing_parameters['aggregate_data']['sample'])
        self.cost_parameters = cost_parameters

        self.sample_cost = {}
        self._sample_cost = []

    @staticmethod
    def _exponential(
            value: float,
            max_value: float,
            exponent: float = 2,
    ):
        """
        Exponential decay function.
        :param value: The value to evaluate
        :param max_value: The maximum possible value
        :param exponent: The rate of decay
        :return: The result of the evaluation of the exponential decay function
        """
        return 1 - (value / max_value) ** exponent

    @staticmethod
    def _logistic(
            value: float,
            midpoint: float = 0,
            maximum: float = 1,
            rate: float = 1,
    ):

        """
        Logistic funciton.  see: https://en.wikipedia.org/wiki/Logistic_function
        This function has been modified so that it decays, instead of grow.
        :param value: The value to evaluate
        :param midpoint: The sigmoid's midpoint
        :param maximum: The curve's maximum value
        :param rate: The growth rate of the function
        :return: The result of the evaluation of the Logistic function
        """

        return maximum * (1 - 1 / (1 + np.exp(- rate * (value - midpoint))))

    def cost(self):

        df = pd.DataFrame()

        for key, value in self.cost_parameters.items():

            col_name = value['value']

            if col_name in self.sample_df.columns:

                if value['response'] == 'logistic':
                    new_name = f'{col_name}_logistic'
                    df[new_name] = self.sample_df[col_name].apply(self._logistic,
                                                                  midpoint=value['midpoint'],
                                                                  maximum=value['maximum'],
                                                                  rate=value['rate'])

                elif value['response'] == 'linear':
                    new_name = f'{col_name}_linear'
                    df[new_name] = self.sample_df[col_name]

                elif value['response'] == 'exponential':
                    new_name = f'{col_name}_exponential'
                    df[new_name] = self.sample_df[col_name].apply(self._exponential,
                                                                  max_value=value['max_value'],
                                                                  exponent=value['exponent'])
                else:
                    logger.warning(f"Invalid cost term: {value}")

        df['cost'] = df.sum(axis=1)
        df['sample'] = self.sample_df['sample']
        df = pd.merge(df, self.sample_df, on='sample')

        return df

    def function_cost(self):
        """
        The cost function is the response of the measured values to be maximized by ChemOS.
        :param kwargs: Each term of the cost function for a sample
        :return: cost of the sample given the result of the measurement
        """

        if self.sample_number is None:
            raise Exception("Can only process cost for a single sample. Please define a sample to process.")

        cost_response = []

        for key, value in self.cost_parameters.items():

            # Logistic response
            if value['response'] == 'logistic':
                value.pop('response')
                value['value'] = self.sample_df[value['value']][0]

                cost_response.append(self._logistic(**value))

            # Exponential response
            elif value['response'] == 'exponential':
                value.pop('response')
                value['value'] = self.sample_df[value['value']][0]

                cost_response.append(self._exponential(**value))

            # linear response
            elif value['response'] == 'linear':
                value.pop('response')
                value['value'] = self.sample_df[value['value']][0]

                cost_response.append(value['value'])
            elif value['response'] == 'custom':
                eq = value['value']

                for p in eq.split(" "):
                    if ';' in p:
                        raise ValueError("no semicolons allowed")
                    if p in self.sample_df.columns:
                        eq = eq.replace(p, str(self.sample_df[p].values[0]))
                    else:
                        try:
                            float(p)
                        except ValueError:
                            if p in ["(", ")", "+", "-", "*", "/"]:
                                continue
                            else:
                                raise ValueError(f"invalid string in equation: {p}")

                # evaluate the string expression
                logger.info(f"COST FUNCTION: custom cost function: {eq}")
                cost_response.append(eval(eq, {'__builtins__': None}))
            else:
                logger.warning(f"Invalid cost term: {value}")

        cost = np.prod(cost_response)

        self._sample_cost.append(cost)

        for i, cost in enumerate(self._sample_cost):
            self.sample_cost[i] = cost

        return cost


if __name__ == "__main__":
    with open(
            '/Users/teddyhaley/PycharmProjects/data_processing/test_data/results_data/2019-08-22_10-47-11/processing_parameters.json',
            'r') as f:
        processing_parameters = json.load(f)

    cost_params = CostParameters(
        pseudomobility={'response': 'linear',
                        'value': 'pseudomobility'},

        anneal={'response': 'exponential',
                'value': 'value_annealing_time',
                'max_value': 300,
                'exponent': 1},
    )

    c = CostFunction(processing_parameters=processing_parameters,
                     cost_parameters=cost_params.dictionary()).cost()

    print(c)
