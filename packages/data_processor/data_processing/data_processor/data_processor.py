import json
import os
from pathlib import Path
import data_processing.data_processor.cost_function_builder as cfb
import importlib


class DataProcessing:
    """
    Constructor for the data pipeline
    """

    def __init__(self,
                 pipeline_name: str,
                 campaign_directory: str,
                 results_directory: str,
                 sample: list = None,
                 workers: int = 1,
                 cost_parameters: dict = None,
                 development=False,
                 docker: bool = False,
                 docker_cache=True,
                 **kwargs
                 ):

        """
        Initialize the Data Processing instance. This sets up the basic information required for the data pipeline
        to ingest.

        :param pipeline_name: Specifies what pipeline to execute in the run_pipeline method
        :param campaign_directory: Directory to the campaign folder to process
        :param results_directory: Directory to output the results of the data pipeline
        :param sample: Tells the pipeline which sample to process
        :param workers: Number of threads to use when executing the data pipeline.
        :param cost_parameters: Dictionary specifying the criteria for manipulating the cost function
        :param development: Run pipeline from subpackage for development purposes
        :param docker: Boolean whether or not to execute the pipeline using docker
        :param docker_cache: Load in the docker container already in memory.
        """

        self.pipeline_name = pipeline_name
        self.campaign_directory = campaign_directory
        self.results_directory = results_directory
        self.cost_parameters = cost_parameters
        self.sample = sample
        self.workers = workers
        self.development = development
        self.docker = docker
        self.docker_cache = docker_cache

        self.campaign_name = None
        self.campaign_results_directory = None
        self.processing_parameters_directory = None
        self.processed_data_directory = None

        self.__dict__.update(kwargs)

        # Build the local directories
        self.processing_setup()

        # Execute the pipeline using docker and mount the local volumes.
        if docker:
            self.run_docker_pipeline()

        # Installs the dependencies for the desired pipeline in this environment. Must install the pipeline.
        else:
            self.pipe = self.run_pipeline()

    def processing_setup(self):
        """
        Saves the data processing criteria into a processing parameters JSON file. This file is used to track the
        output of the tasks from the pipeline.

        :return:
        """
        self.campaign_name = os.path.basename(self.campaign_directory)

        # Make results directory if it doesn't already exist
        self.campaign_results_directory = os.path.join(self.results_directory, 'data_pipeline')
        if not os.path.exists(self.campaign_results_directory):
            os.makedirs(self.campaign_results_directory)

        self.processing_parameters_directory = os.path.join(self.campaign_results_directory,
                                                            'processing_parameters.json')

        if os.path.exists(self.processing_parameters_directory):
            os.remove(self.processing_parameters_directory)

        # Make processed data directory if it doesn't already exist
        self.processed_data_directory = os.path.join(self.campaign_results_directory, "processed_data")
        if not os.path.exists(self.processed_data_directory):
            os.makedirs(self.processed_data_directory)

        with open(self.processing_parameters_directory, 'w') as outfile:
            json.dump(self.__dict__, outfile, indent=4)

    def run_pipeline(self):
        """
        This method determines which pipeline to execute.

        :return: Luigi Pipeline object
        """

        if self.development:
            pipeline = importlib.import_module(
                'packages.' + self.pipeline_name + '.data_processing.' + self.pipeline_name + '.pipeline')
            pipe = pipeline.build_pipeline(parameter_dict=self.__dict__)

        else:
            pipeline = importlib.import_module('data_processing.' + self.pipeline_name + '.pipeline')
            pipe = pipeline.build_pipeline(parameter_dict=self.__dict__)

        return pipe

    def run_docker_pipeline(self):
        """
        This method executes the pipelines in a docker container to avoid conflicts with dependencies.
        :return:
        """

        # When executing this from another script, get the path of this script to execute the dockerfile.
        script_path = os.path.dirname(os.path.realpath(__file__))
        build_path = os.path.join(script_path, 'docker', self.pipeline_name)

        if os.path.exists(build_path):

            if self.docker_cache:

                # Builds the docker image
                os.system(f'docker build {build_path} -t {self.pipeline_name.lower()}')

            else:
                os.system(f'docker build {build_path} -t {self.pipeline_name.lower()} --no-cache')

            # Executes the docker image using the runner
            os.system(f"docker run -v {Path(self.campaign_directory)}:/data_pipeline -v {Path(self.results_directory)}:/results_directory -v {Path(self.campaign_results_directory)}:/processing_parameters_directory {self.pipeline_name.lower()} python3 /tmp/runner.py")

        else:
            raise Exception('The Dockerfile for this pipeline does not exist.')

    def get_cost(self):
        """
        Get the cost to be used for the optimization function, using the cost parameters dictionary.
        :return: float: cost of the function
        """
        with open(self.processing_parameters_directory) as json_file:
            parameters_dict = json.load(json_file)

        if parameters_dict['sample'] is None:
            raise Exception("Must specify sample to process.")

        if self.cost_parameters is None:
            raise Exception("Must define cost parameters.")

        if self.docker:
            raise Exception("Docker pipelines do not result a pipe.")

        return cfb.CostFunction(processing_parameters=parameters_dict,
                                cost_parameters=self.cost_parameters).function_cost()


if __name__ == '__main__':
    # dp = DataProcessing(
    #     pipeline_name='conductance_image_segmentation_01102020',
    #     campaign_directory='/Users/teddyhaley/Downloads/2020-10-11_18-04-30',
    #     results_directory='/Users/teddyhaley/Downloads/results_new_alg',
    #     # sample=[0],
    #     workers=1,
    #     development=True,
    #     docker=False,
    #     docker_cache=False,
    # )

    # campaigns = ['2020-10-08_18-22-14',
    #              '2020-10-09_17-37-10',
    #              '2020-10-11_18-04-30']
    #
    # result_file = 'fw_5_images'
    #
    # for campaign in campaigns:
    #
    #     os.makedirs(f'/Users/teddyhaley/Downloads/{campaign}/{result_file}')
    #
    #     dp = DataProcessing(
    #         pipeline_name='conductance_image_segmentation_01102020',
    #         campaign_directory=f'/Users/teddyhaley/Downloads/{campaign}',
    #         results_directory=f'/Users/teddyhaley/Downloads/{campaign}/{result_file}',
    #         # sample=[0],
    #         workers=1,
    #         development=True,
    #         docker=False,
    #         docker_cache=False,
    #     )

    # # Bismuth Pipeline
    # dp = DataProcessing(
    #     pipeline_name='bismuth_spincoating_30062020',
    #     campaign_directory="/Users/teddyhaley/Desktop/2020-06-30_14-48-57",
    #     results_directory="/Users/teddyhaley/Desktop/processed",
    #     sample=[0],
    #     workers=1,
    #     development=True,
    #     docker=False,
    #     docker_cache=False,
    #     dye_color='blue'
    # )

    # # PQLE Pipeline
    # dp = DataProcessing(
    #     pipeline_name='PLQE_06052020',
    #     campaign_directory="/Users/teddyhaley/Desktop/PLQE/2020-03-24_18-51-00",
    #     results_directory="/Users/teddyhaley/Desktop/PLQE/2020-03-24_18-51-00/output",
    #     sample=[1],
    #     workers=1,
    #     docker=False
    # )

    # #BPM Nov 30 2020
    # campaign_directory = r'C:\campaign_data\xrf_dummy_dataset\data'
    # results_directory = r'C:\campaign_data\xrf_dummy_dataset\results'
    #
    #
    #
    # dp = DataProcessing(
    #     pipeline_name='combustion_synthesis_30072020',
    #     campaign_directory=campaign_directory,
    #     results_directory=results_directory,
    #     # sample=[0],
    #     workers=1,
    #     development=True,
    #     docker=False,
    #     docker_cache=False,
    # )

    base_path = '/Users/fraserparlane/Desktop/ada-chem/data_processing/tests/test_data/campaign/2020-11-30_17-22-26_'

    # Execute pipeline
    proc = DataProcessing(
        pipeline_name='combustion_synthesis_30072020',
        campaign_directory=f'{base_path}campaign',
        results_directory=f'{base_path}results',
        workers=1,
        development=True,
        docker=False,
        docker_cache=False,
    )
