from data_processing.data_processor.luigi_task_wrapper import *
from data_processing.data_processor.pipe_line_base_fuctions import CampaignParameters, get_largest_numbered_file_name, get_smallest_numbered_file_name
import numpy as np
import pandas as pd
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import math


def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.2989, 0.5870, 0.1140])


def calc_rms_noise(image):
    area_average = image.mean()
    return math.sqrt(((image - area_average) ** 2).mean())


def calc_mean_noise(image):
    area_average = image.mean()
    return np.abs(image - area_average).mean()


class ProcessImageNoise(TaskTemplate):
    """
    This task runs the image noise algorithms on the sample image.
    """

    def requires(self):
        return CampaignParameters()

    def run(self):

        self.get_parameters_dict()
        with open(self.parameters_dict['config_directory']) as f:
            config_dict = json.load(f)

        LoggingConfig.logger.info("running ProcessImageNoise")

        try:
            noise_data = []
            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():
                sample_number = int(sample_number)
                if 'FLIR_CAMERA' not in os.listdir(sample_directory):
                    raise Exception('FLIR_CAMERA directory doesnt exist')
                else:
                    images_directory = os.path.join(sample_directory, 'FLIR_CAMERA')

                LoggingConfig.logger.info(
                    f"{self._get_classname()} - Processing Images for sample {sample_number}")
                images = [
                    {
                        "suffix": "initial",
                        "file_name": get_smallest_numbered_file_name(file_path=images_directory, ext="png"),
                        "crop_rect": config_dict["FLIR_CAMERA"]["full_crop"]
                    },
                    {
                        "suffix": "final",
                        "file_name": get_largest_numbered_file_name(file_path=images_directory, ext="png"),
                        "crop_rect": config_dict["FLIR_CAMERA"]["processing_crop"]
                    }
                ]
                noise_rms = []
                noise_mean = []
                for image in images:
                    img = mpimg.imread(image["file_name"])
                    crop_rect = image["crop_rect"]
                    img_cropped = img[crop_rect[2]:crop_rect[3], crop_rect[0]:crop_rect[1]]
                    gray = rgb2gray(img_cropped)
                    plt.imsave(os.path.join(self.parameters_dict['processed_data_directory'], f"noise_{image['suffix']}.png"), gray)

                    total_rms = calc_rms_noise(gray)
                    LoggingConfig.logger.info(f"{self._get_classname()}total rms noise = {total_rms}")
                    noise_rms.append(total_rms)

                    total_mean = calc_mean_noise(gray)
                    LoggingConfig.logger.info(f"{self._get_classname()}total mean noise = {total_mean}")
                    noise_mean.append(total_mean)

                data_dict = {"sample": sample_number, "image_noise_rms_initial": noise_rms[0], "image_noise_mean_initial": noise_mean[0], "image_noise_rms_final": noise_rms[1], "image_noise_mean_final": noise_mean[1]}
                noise_data.append(data_dict)
            data_df = pd.DataFrame(noise_data, index=[0])
            data_save_path = os.path.join(self.parameters_dict['processed_data_directory'], "sample_image_noise.csv")
            data_df.to_csv(data_save_path, index=False)
            self.parameters_dict['sample_data']['image_noise'] = data_save_path
            self.save_parameters_dict()

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} -  processing failed")

        self.save_parameters_dict()
