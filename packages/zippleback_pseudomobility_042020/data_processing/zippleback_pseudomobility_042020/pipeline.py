from data_processing.data_processor.luigi_task_wrapper import *
from luigi.mock import MockFileSystem
import shutil
import os
import pandas as pd
import numpy as np
import data_processing.data_processor.cost_function_builder as cfb
import data_processing.data_processor.luigi_helper_functions as lhf
from functools import reduce

# Imaging analysis
import warnings

# ignore tensorflow warnings
warnings.filterwarnings('ignore', module=".*tensorflow.*")

from ada_imaging.crack_detection.labelling import ImageClassifier

from ada_toolbox.data_analysis_lib import image_analysis as img_analysis

# Conductivity analysis
from ada_toolbox.data_analysis_lib import conductivity_analysis as cond

# Spectroscopy analysis
from ada_toolbox.analytical_lib import analytical_tools as m

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


class CampaignParameters(TaskTemplate):
    """
    This Task sets up the data processing pipeline. This task reads in the campaign parameters JSON and converts it into
    a chemical attributes CSV.
    """

    def run(self):

        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self._get_classname()} - Reading Campaign Parameters")

        # instantiate dict keys
        self.parameters_dict['raw_data'] = {}
        self.parameters_dict['sample_data'] = {}
        self.parameters_dict['position_data'] = {}
        self.parameters_dict['aggregate_data'] = {}
        self.parameters_dict['image_directory'] = {}
        self.parameters_dict['sample_directories'] = {}

        # Get sample directories in the raw data folder
        campaign_files = os.listdir(self.parameters_dict['campaign_directory'])
        requested_samples = self.parameters_dict.get('sample', None)

        if "config.json" in campaign_files:
            file_directory = os.path.join(self.parameters_dict['campaign_directory'], "config.json")
            self.parameters_dict['config_directory'] = file_directory
        else:
            self.parameters_dict['config_directory'] = None

        if "campaign_parameters.json" in campaign_files:
            file_directory = os.path.join(self.parameters_dict['campaign_directory'], "campaign_parameters.json")
            self.parameters_dict['campaign_parameters_directory'] = file_directory
        else:
            self.parameters_dict['campaign_parameters_directory'] = None

        # if labels.json exists save the output
        p = os.path.join(self.parameters_dict.get('campaign_directory', ''), 'labels.json')
        if os.path.exists(p):
            labels = os.path.join(self.parameters_dict['processed_data_directory'], "labels.json")
            self.parameters_dict['labels'] = labels
            shutil.copy(p, labels)

        for file in campaign_files:

            file_directory = os.path.join(self.parameters_dict['campaign_directory'], file)

            if file.startswith('sample_') and \
                    os.path.isdir(file_directory) and \
                    len(os.listdir(file_directory)) >= 1:

                file_number = int(file.split("_")[1])

                if requested_samples is None:
                    self.parameters_dict['sample_directories'][file_number] = file_directory

                else:

                    if file_number in requested_samples:
                        self.parameters_dict['sample_directories'][file_number] = file_directory

                    else:
                        pass

        self.save_parameters_dict()


class ProcessImages(TaskTemplate):
    """
    This task runs the image classification algorithms on the sample images.
    """

    def requires(self):
        return CampaignParameters()

    def run(self):

        self.get_parameters_dict()

        image_scores_list = []
        image_ctrl_index = self.parameters_dict.get('image_ctrl_index', None)

        try:
            # Create a CSV of the experiment dictionary for each sample. Map JSON to DF.
            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():
                sample_number = int(sample_number)
                if 'IMAGES' not in os.listdir(sample_directory):
                    raise Exception('IMAGES directory doesnt exist')
                else:
                    images_directory = os.path.join(sample_directory, 'IMAGES')

                image_files = os.listdir(images_directory)
                image_files = [file for file in image_files if file.endswith('.jpg')]
                image_files.sort()

                self.parameters_dict['image_directory'][sample_number] = {}

                LoggingConfig.logger.info(
                    f"{self._get_classname()} - Processing Images for sample {sample_number}")
                if image_ctrl_index is not None:
                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Using control index to determine which image file to use for processing")
                    image_files = [file for file in image_files if file.startswith(
                        '{:03d}'.format(image_ctrl_index))]
                else:
                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - No control index given - using the last file in the list - {image_files[-1]}")
                    image_files = [image_files[-1]]

                for image in image_files:
                    # parse filename
                    ctrl_index, equipment, other, position = lhf.file_name_parser(image)

                    LoggingConfig.logger.info(f"{self._get_classname()} - Calculating cost based on {image} image")
                    image_file = os.path.join(images_directory, image)

                    image_scores = {}

                    # CNN image processing
                    classifier = ImageClassifier()
                    for model_name in classifier.models.keys():
                        LoggingConfig.logger.info(
                            f"{self._get_classname()} - Generating {model_name} score for image: {image_file}")
                        image_scores[f'{model_name}_score'] = float(classifier.label_image(model_name, image_file)[0])

                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Generating blur and brightness scores for image: {image_file}")
                    image_scores['blur_score'] = img_analysis.blur_detection(image_directory=image_file)
                    image_scores['brightness_score'] = img_analysis.brightness_detection(image_directory=image_file)
                    image_scores['sample'] = int(sample_number)
                    image_scores['position'] = int(position)

                    self.parameters_dict['image_directory'][sample_number][ctrl_index] = image_file

                    # Convert the dictionary to a dataframe and save as CSV
                    image_scores_df = pd.DataFrame.from_dict(image_scores, orient='index')
                    image_df_transpose = image_scores_df.T
                    image_scores_list.append(image_df_transpose)

            # Save position scores
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving position image data")
            image_df_total = pd.concat(image_scores_list, ignore_index=True)
            image_scores_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                  "position_image_scores.csv")
            image_df_total.to_csv(image_scores_directory, index=False)
            self.parameters_dict['position_data']['image_scores'] = image_scores_directory

            # Save sample scores
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving sample image data")
            sample_df = image_df_total.groupby(['sample']).mean()
            sample_df.reset_index(level=0, inplace=True)
            del sample_df['position']
            image_sample_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                  "sample_image_scores.csv")
            sample_df.to_csv(image_sample_directory, index=False)
            self.parameters_dict['sample_data']['image_scores'] = image_sample_directory

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} -  processing failed")

        self.save_parameters_dict()


class ProcessConductivity(TaskTemplate):
    """
    This task calculates the conductance of the given data by position.
    """

    def requires(self):
        return CampaignParameters()

    def run(self):

        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self._get_classname()} - Processing Conductivity")

        raw_conductivity_list = []
        processed_conductivity_list = []
        units = {}

        conductivity_ctrl_index = self.parameters_dict.get('conductivity_ctrl_index', None)

        try:
            # Create a CSV of the experiment dictionary for each sample. Map JSON to DF.
            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():
                sample_number = int(sample_number)
                if 'CONDUCTIVITY' not in os.listdir(sample_directory):
                    raise Exception('CONDUCTIVITY directory doesnt exist')
                else:
                    conductivity_directory = os.path.join(sample_directory, 'CONDUCTIVITY')

                conductivity_position_files = os.listdir(conductivity_directory)
                conductivity_position_files = [file for file in conductivity_position_files if file.endswith('.csv')]
                conductivity_position_files.sort()

                for file in conductivity_position_files:
                    ctrl_index, equipment, other, position = lhf.file_name_parser(file)
                    position_number = int(position)

                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Processing Conductivity for sample {sample_number}, position {position_number}")
                    if conductivity_ctrl_index is not None:
                        LoggingConfig.logger.info(
                            f"{self._get_classname()} - Using control index to determine which conductivity file to use for calculations")
                        conductivity_files = [file for file in conductivity_position_files if
                                              file.endswith(position + '.csv') and file.startswith(
                                                  '{:03d}'.format(conductivity_ctrl_index))]
                    else:
                        LoggingConfig.logger.info(
                            f"{self._get_classname()} - No control index given - using the default file")
                        conductivity_files = [file for file in conductivity_position_files if
                                              file.endswith(position + '.csv')]

                    # There should only be a single conductivity file per position.
                    if len(conductivity_files) == 1:
                        conductivity_file_directory = os.path.join(conductivity_directory, conductivity_files[0])
                        conductivity_df = pd.read_csv(conductivity_file_directory)
                        conductivity_df['sample'] = sample_number
                        conductivity_df['position'] = position_number

                        # Aggregate all raw positional data
                        raw_conductivity_list.append(conductivity_df)

                        # process the conductivity data
                        # Remove saturated data points. Saturation thresholds are defined in the toolbox.
                        df_rs = cond.remove_saturated_points(data=conductivity_df)

                        # Fit RANSAC robust linear model to the data. Minimum valid points defined in toolbox.
                        r2, slope, intercept = cond.linear_regression_RANSAC(data=df_rs,
                                                                             x_axis="Voltage",
                                                                             y_axis="Current")
                        processed_dict = {
                            'sample': sample_number,
                            'position': position_number,
                            # 'r2': r2,
                            'conductance': slope,
                            # 'intercept': intercept
                        }

                        units['r2'] = {'r2': 'siemens'}
                        units['intercept'] = {'intercept': 'siemens'}
                        units['conductance'] = {'conductance': 'siemens'}

                        processed_df = pd.DataFrame.from_dict(processed_dict, orient='index')
                        transpose_processed_df = processed_df.T
                        processed_conductivity_list.append(transpose_processed_df)

            # Combine the dataframes from each sample and save the data
            raw_conductivity_total = pd.concat(raw_conductivity_list)
            processed_conductivity_total = pd.concat(processed_conductivity_list)

            LoggingConfig.logger.info(f"{self._get_classname()} - Saving position conductivity data")
            processed_conductivity_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                            "position_conductivity.csv")
            processed_conductivity_total.to_csv(processed_conductivity_directory, index=False)
            self.parameters_dict['position_data']['conductivity'] = processed_conductivity_directory

            # Save sample conductivity
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving sample conductivity data")
            processed_sample_conductivity_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                                   "sample_conductivity.csv")
            sample_df = processed_conductivity_total.groupby(['sample']).mean()
            sample_df.reset_index(level=0, inplace=True)
            del sample_df['position']
            sample_df.to_csv(processed_sample_conductivity_directory, index=False)
            self.parameters_dict['sample_data']['conductivity'] = processed_sample_conductivity_directory

            LoggingConfig.logger.info(f"{self._get_classname()} - Saving raw conductivity data")
            raw_conductivity_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                      "raw_conductivity.csv")
            raw_conductivity_total.to_csv(raw_conductivity_directory, index=False)
            self.parameters_dict['raw_data']['conductivity'] = raw_conductivity_directory
        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} -  processing failed")

        self.save_parameters_dict()


class ProcessSpectroscopy(TaskTemplate):
    '''
    This task processes the spectroscopy data by position. To calculate the height of the gaussian for a sample, we must
    process 4 files: blank reflectance, blank transmission, reflectance, and transmission.
    Blank reflectance and transmission are only found in the first sample for a given campaign. As such, if we are only
    processing a single campaign, we must back track through the samples to find the blank reflectance and transmission
    data.
    '''

    def requires(self):
        return CampaignParameters()

    def run(self):

        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self._get_classname()} - Processing Spectroscopy")

        processed_spectra_list = []
        raw_spectra_list = []
        units = {}

        spectroscopy_ctrl_index = self.parameters_dict.get('spectroscopy_ctrl_index', None)

        try:
            # Create a CSV of the experiment dictionary for each sample.
            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():
                sample_number = int(sample_number)
                blank_spectra = {}

                if 'UVVIS' not in os.listdir(sample_directory):
                    raise Exception('UVVIS directory doesnt exist')
                else:
                    spectra_directory = os.path.join(sample_directory, 'UVVIS')

                blank_files_all_pos = [file for file in os.listdir(spectra_directory) if
                                       'blank' in file and file.endswith('.csv')]
                blank_files_all_pos.sort()

                # determine which files to use for calculations
                if spectroscopy_ctrl_index is not None:
                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Using control index to determine which spectroscopy file to use for calculations")
                    spectra_files_all_pos = [file for file in os.listdir(spectra_directory) if file.startswith(
                        '{:03d}'.format(spectroscopy_ctrl_index)) and 'blank' not in file]
                else:
                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - No control index given - using the default file")
                    spectra_files_all_pos = [file for file in os.listdir(spectra_directory) if 'blank' not in file]

                spectra_files_all_pos.sort()

                # remove .ini files
                spectra_files_all_pos = [x for x in spectra_files_all_pos if not x.endswith('.ini')]

                # get number  of unique positions
                positions = [lhf.file_name_parser(filename)[3] for filename in spectra_files_all_pos]
                positions = list(set(positions))
                positions.sort()

                for position in positions:
                    position_number = int(position)
                    blank_spectra[position_number] = {}

                    spectra_pos = [file for file in spectra_files_all_pos if
                                   lhf.file_name_parser(file)[3] == position and file.endswith('.csv')]
                    spectra_blank_pos = [file for file in blank_files_all_pos if
                                         lhf.file_name_parser(file)[3] == position]

                    if not spectra_blank_pos:
                        raise Exception('Blank slide data NOT FOUND in sample directory')
                    elif len(spectra_pos) != len(spectra_blank_pos):
                        raise ValueError('Missmatched number of blank spectra files and sample spectra files')
                    elif len(spectra_pos) != 2:
                        raise Exception(f"Must have a single reflectance file and single transmission data file.")

                    for file in spectra_blank_pos:
                        if '_t' in file:
                            blank_transmission_directory = os.path.join(spectra_directory, file)
                            blank_transmission = pd.read_csv(blank_transmission_directory)
                            blank_transmission['sample'] = sample_number
                            blank_transmission['position'] = position_number
                            blank_transmission['r-t'] = 't'
                            blank_transmission['blank'] = True

                            blank_spectra[position_number]['t'] = blank_transmission
                        else:
                            blank_reflectance_directory = os.path.join(spectra_directory, file)
                            blank_reflectance = pd.read_csv(blank_reflectance_directory)
                            blank_reflectance['sample'] = sample_number
                            blank_reflectance['position'] = position_number
                            blank_reflectance['r-t'] = 'r'
                            blank_reflectance['blank'] = True

                            blank_spectra[position_number]['r'] = blank_reflectance

                    if blank_spectra is None:
                        raise Exception(f"Missing blank reflectance or transmission data in SAMPLE {sample_number}")

                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Processing Spectroscopy for sample {sample_number}, position {position_number}")

                    for file in spectra_pos:
                        if '_t' in file:
                            transmission_directory = os.path.join(spectra_directory, file)
                            transmission = pd.read_csv(transmission_directory)
                            transmission['sample'] = sample_number
                            transmission['position'] = position_number
                            transmission['r-t'] = 't'
                            transmission['blank'] = False
                        else:
                            reflectance_directory = os.path.join(spectra_directory, file)
                            reflectance = pd.read_csv(reflectance_directory)
                            reflectance['sample'] = sample_number
                            reflectance['position'] = position_number
                            reflectance['r-t'] = 'r'
                            reflectance['blank'] = False

                    r_0 = blank_reflectance['calibrated_signal']
                    t_0 = blank_transmission['calibrated_signal']
                    r_1 = reflectance['calibrated_signal']
                    t_1 = transmission['calibrated_signal']
                    x_nm = reflectance['x_(nm)']

                    # Calculate absorbance of film
                    absorbance_df = m.calculate_film_absorbance(r_0, t_0, r_1, t_1, x_nm)

                    # calc_absorbance_from_t_and_r
                    a_data = m.calc_absorbance_from_t_and_r(t_data=transmission, r_data=reflectance)
                    a_data = a_data.rename(
                        columns={'calibrated_signal': 'absorbance',
                                 'calibrated_signal_smoothed': 'absorbance_smoothed'})

                    # Set regions for gaussian
                    default_gaussian = [m.add_gaussian_region(name='a', upper_bound=2.5, lower_bound=2.45)]

                    # Initialize gaussian
                    default_gaussian = m.add_gaussian_to_region(gaussian_bounds_ev=default_gaussian, region_name='a',
                                                                a=1.1, b=4.2, c=0.2)

                    # Select the gaussian dictionary (list of 1)
                    default_gaussian = default_gaussian[0]

                    # Find the log absorbance within the boundaries
                    y_range_abs = absorbance_df['log_A_f'][
                        (absorbance_df['energy_(eV)'] < default_gaussian['bounds_ev'][0]) &
                        (absorbance_df['energy_(eV)'] > default_gaussian['bounds_ev'][1])]

                    height_gaussian = np.mean(y_range_abs)
                    units['height_gaussian'] = {'height_gaussian': 'unitless'}

                    spectra_dict = {
                        'sample': sample_number,
                        'position': position_number,
                        'height_gaussian': height_gaussian
                    }

                    # Append processed data to larger dataframe
                    processed_df = pd.DataFrame.from_dict(spectra_dict, orient='index')
                    transpose_processed_df = processed_df.T
                    processed_spectra_list.append(transpose_processed_df)

                    # Combine the data
                    pos_raw = pd.concat([blank_transmission, blank_reflectance, reflectance, transmission])
                    combined_raw = pd.merge(pos_raw, absorbance_df, on=['x_(nm)'])
                    combined_absorb = pd.merge(combined_raw, a_data, on=['x_(nm)'])
                    del combined_absorb['Unnamed: 0']
                    del combined_absorb['calibrated_signal_smoothed']
                    del combined_absorb['baseline_dark_smoothed']
                    del combined_absorb['baseline_light_smoothed']
                    del combined_absorb['raw_signal_smoothed']
                    del combined_absorb['absorbance_smoothed']

                    raw_spectra_list.append(combined_absorb)

            # Aggregate raw and processed data
            processed_total = pd.concat(processed_spectra_list)

            # Save the position data
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving position spectra data")
            processed_spectra_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                       "position_spectra.csv")
            processed_total.to_csv(processed_spectra_directory, index=False)
            self.parameters_dict['position_data']['spectroscopy'] = processed_spectra_directory

            # Save the position data
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving sample spectra data")
            processed_sample_spectra_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                              "sample_spectra.csv")
            sample_df = processed_total.groupby(['sample']).mean()
            sample_df.reset_index(level=0, inplace=True)
            del sample_df['position']
            sample_df.to_csv(processed_sample_spectra_directory, index=False)
            self.parameters_dict['sample_data']['spectroscopy'] = processed_sample_spectra_directory

            raw_total = pd.concat(raw_spectra_list)
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving raw spectra data")
            raw_spectra_directory = os.path.join(self.parameters_dict['processed_data_directory'], "raw_spectra.csv")
            raw_total.to_csv(raw_spectra_directory, index=False)
            self.parameters_dict['raw_data']['spectroscopy'] = raw_spectra_directory
            del raw_total

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")

        self.save_parameters_dict()


class CalculatePseudofret(TaskTemplate):
    '''
    This task uses the spectroscopy to calculate pseudofret.
    '''

    def requires(self):
        return ProcessSpectroscopy()

    def run(self):

        self.get_parameters_dict()

        try:
            raw_spectra_directory = self.parameters_dict['raw_data'].get('spectroscopy', None)

            if raw_spectra_directory is not None:
                spectra_df = pd.read_csv(raw_spectra_directory)

        except:
            LoggingConfig.logger.error(f"{self._get_classname()} - error")

        LoggingConfig.logger.info(f"{self._get_classname()} - Calculating pesudofret")

        try:

            o = []
            for name, gdf in spectra_df.groupby("sample"):
                score = m.pseudofret(gdf)
                o.append(dict(pseudofret=score, sample=name))

            pfret = pd.DataFrame(o)
            pfret.sort_values(by="sample", inplace=True)

            # save the processed data
            sample_pseudofret_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                       'sample_pseudofret.csv')

            self.parameters_dict['sample_data']['pseudofret'] = sample_pseudofret_directory
            pfret.to_csv(sample_pseudofret_directory, index=False)
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving sample pseudofret data")

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")

        self.save_parameters_dict()


class CalculatePseudomobility(TaskTemplate):
    '''
    This task combines the spectroscopy and conductivity subpipes. Pseudomobility for a sample is calculated by getting
    the pseudomobility by position and then taking the average of the positions for the sample.
    '''

    def requires(self):
        yield [
            ProcessSpectroscopy(),
            ProcessConductivity()
        ]

    def run(self):

        self.get_parameters_dict()

        # Get the processed spectroscopy and conducutivity dataframe directories
        processed_spectra_directory = self.parameters_dict['sample_data'].get('spectroscopy', None)
        processed_conductivity_directory = self.parameters_dict['sample_data'].get('conductivity', None)

        if processed_spectra_directory is not None:
            specta_df = pd.read_csv(processed_spectra_directory)

        if processed_conductivity_directory is not None:
            conductivity_df = pd.read_csv(processed_conductivity_directory)

        try:
            LoggingConfig.logger.info(f"{self._get_classname()} - Calculating pseudomobility")

            # Merge the conductance and height of the gaussian dataframes by position and sample.
            sample_pseudo_df = pd.merge(specta_df, conductivity_df, on=['sample'])

            # Calculate positional pseudomobility
            sample_pseudo_df['pseudomobility'] = sample_pseudo_df['conductance'] / sample_pseudo_df[
                'height_gaussian']

            del sample_pseudo_df['conductance']
            del sample_pseudo_df['height_gaussian']

            # Save the processed sample pseudomobility data
            sample_pseudomobility_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                           'sample_pseudomobility.csv')
            sample_pseudo_df.to_csv(sample_pseudomobility_directory, index=False)
            self.parameters_dict['sample_data']['pseudomobility'] = sample_pseudomobility_directory
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving sample pseudomobility data")

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")

        self.save_parameters_dict()


class AggregateData(TaskTemplate):
    '''
    Aggregate position and sample data.
    '''

    def requires(self):
        yield [
            ProcessImages(),
            CalculatePseudomobility(),
            CalculatePseudofret(),
        ]

    def run(self):

        self.get_parameters_dict()

        sample_df_list = []
        for sample_directory in self.parameters_dict['sample_data'].values():
            try:
                sample_df = pd.read_csv(sample_directory)
                sample_df_list.append(sample_df)
            except pd.errors.EmptyDataError:
                pass

        combined_df = reduce(lambda left, right: pd.merge(left, right, on='sample'), sample_df_list)

        LoggingConfig.logger.info(f"{self._get_classname()} - Saving aggregate sample data")
        aggregate_sample_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                  'sample_aggregate.csv')
        combined_df.to_csv(aggregate_sample_directory, index=False)
        self.parameters_dict['aggregate_data']['sample'] = aggregate_sample_directory

        self.save_parameters_dict()


class CustomTransformations(TaskTemplate):

    def requires(self):
        return AggregateData()

    def run(self):

        self.get_parameters_dict()

        # Generate a dataframe with the transformed variables with cost
        cost_parameters_dict = self.parameters_dict.get('cost_parameters', None)
        if cost_parameters_dict is None:
            LoggingConfig.logger.info("No cost parameters provided. Cost not calculated.")

        else:
            sample_cost_directory = os.path.join(self.parameters_dict['processed_data_directory'], 'sample_cost.csv')
            self.parameters_dict['sample_cost'] = sample_cost_directory
            cost_df = cfb.CostFunction(processing_parameters=self.parameters_dict,
                                       cost_parameters=cost_parameters_dict).cost()
            cost_df.to_csv(sample_cost_directory, index=False)

        self.save_parameters_dict()


class DataEntry(TaskTemplate):
    '''
    Data is entered at the last task in the pipeline.
    '''

    def requires(self):
        # passes the dir dictionary up to the preceding task.
        return CustomTransformations()

    def run(self):
        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self._get_classname()} - Complete the pipeline")

        self.save_parameters_dict()


def build_pipeline(parameter_dict):
    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    # Every luigi tasks inherits the processing parameters dictionary and the logger
    Parameters(dictionary=parameter_dict)
    LoggingConfig(campaign_results_directory=parameter_dict['campaign_results_directory'])

    pipe = luigi.build(
        [
            DataEntry(),
        ],
        local_scheduler=True,
        workers=parameter_dict['workers']
    )

    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    return pipe
