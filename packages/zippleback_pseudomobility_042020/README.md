## Installing

```
pip install "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.zippleback_pseudomobility_042020&subdirectory=packages/zippleback_pseudomobility_042020""
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.zippleback_pseudomobility_042020&subdirectory=packages/zippleback_pseudomobility_042020"
```
