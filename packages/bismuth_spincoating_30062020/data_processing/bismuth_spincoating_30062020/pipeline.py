from data_processing.data_processor.luigi_task_wrapper import *
from luigi.mock import MockFileSystem
import shutil
import os
import pandas as pd
import data_processing.data_processor.cost_function_builder as cfb
import data_processing.data_processor.luigi_helper_functions as lhf
from functools import reduce
import numpy as np

# Imaging analysis
import warnings
from ada_imaging.film_coverage.coverage import get_coverage
from ada_imaging.film_thickness.thickness import get_thickness

# ignore tensorflow warnings
warnings.filterwarnings('ignore', module=".*tensorflow.*")

pd.set_option('display.max_rows', 1000)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


class CampaignParameters(TaskTemplate):
    """
    This Task sets up the data processing pipeline. This task reads in the campaign parameters JSON and converts it into
    a chemical attributes CSV.
    """

    def run(self):

        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self.__class__.__name__} - Reading Campaign Parameters")

        # instantiate dict keys
        self.parameters_dict['raw_data'] = {}
        self.parameters_dict['sample_data'] = {}
        self.parameters_dict['measurement_data'] = {}
        self.parameters_dict['aggregate_data'] = {}
        self.parameters_dict['image_directory'] = {}
        self.parameters_dict['processed_image_directory'] = {}
        self.parameters_dict['sample_directories'] = {}

        # Get sample directories in the raw data folder
        campaign_files = os.listdir(self.parameters_dict['campaign_directory'])
        requested_samples = self.parameters_dict.get('sample', None)

        if "config.json" in campaign_files:
            file_directory = os.path.join(self.parameters_dict['campaign_directory'], "config.json")
            self.parameters_dict['config_directory'] = file_directory
        else:
            self.parameters_dict['config_directory'] = None

        if "campaign_parameters.json" in campaign_files:
            file_directory = os.path.join(self.parameters_dict['campaign_directory'], "campaign_parameters.json")
            self.parameters_dict['campaign_parameters_directory'] = file_directory
        else:
            self.parameters_dict['campaign_parameters_directory'] = None

        # if labels.json exists save the output
        p = os.path.join(self.parameters_dict.get('campaign_directory', ''), 'labels.json')
        if os.path.exists(p):
            labels = os.path.join(self.parameters_dict['processed_data_directory'], "labels.json")
            self.parameters_dict['labels'] = labels
            shutil.copy(p, labels)

        for file in campaign_files:

            file_directory = os.path.join(self.parameters_dict['campaign_directory'], file)

            if file.startswith('sample_') and \
                    os.path.isdir(file_directory) and \
                    len(os.listdir(file_directory)) > 1:

                file_number = int(file.split("_")[1])

                if requested_samples is None:
                    self.parameters_dict['sample_directories'][file_number] = file_directory

                else:
                    if file_number in requested_samples:
                        self.parameters_dict['sample_directories'][file_number] = file_directory
                    else:
                        pass

        self.save_parameters_dict()


class ProcessImages(TaskTemplate):
    """
    This task runs the image classification algorithms on the sample images.
    """

    def requires(self):
        return CampaignParameters()

    def run(self):

        self.get_parameters_dict()

        dye_color = self.parameters_dict.get('dye_color', None)
        image_scores_list = []

        try:

            # Create a CSV of the experiment dictionary for each sample. Map JSON to DF.
            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():

                sample_number = int(sample_number)

                image_folder = os.path.join(sample_directory, 'IMAGES')

                files_in_image_folder = os.listdir(image_folder)

                images = [file for file in files_in_image_folder if file.endswith('.jpg') and 'blank' not in file]
                blank_image = [file for file in files_in_image_folder if file.endswith('.jpg') and 'blank' in file][0]
                blank_image_directory = os.path.join(image_folder, blank_image)

                self.parameters_dict['image_directory'][sample_number] = {}
                self.parameters_dict['processed_image_directory'][sample_number] = {}

                image_id = self.parameters_dict.get('image_id', None)

                for image_number, image in enumerate(images):

                    image_directory = os.path.join(image_folder, image)
                    image_scores = {}

                    ctrl_ix, equip, other, position = lhf.file_name_parser(image)
                    ctrl_ix = int(ctrl_ix)

                    if image_id is not None and image_id in image or len(images) == 1:
                        image_scores['sample'] = sample_number
                        image_scores['measurement'] = ctrl_ix
                        image_scores['coverage'] = get_coverage(blank_image_path=blank_image_directory,
                                                                sample_image_path=image_directory,
                                                                sample=sample_number,
                                                                measurement=ctrl_ix,
                                                                processed_data_directory=self.parameters_dict[
                                                                    'processed_data_directory'])

                        image_scores['thickness'] = get_thickness(sample_image_directory=image_directory,
                                                                  blank_image_directory=blank_image_directory,
                                                                  dye_color=dye_color)

                        self.parameters_dict['image_directory'][sample_number][ctrl_ix] = image_directory
                        self.parameters_dict['processed_image_directory'][sample_number][ctrl_ix] = \
                            os.path.join(self.parameters_dict['processed_data_directory'],
                                         f'processed_image_s{sample_number}_m{ctrl_ix}.jpg')

                    else:
                        image_scores['sample'] = sample_number
                        image_scores['measurement'] = ctrl_ix
                        image_scores['coverage'] = get_coverage(blank_image_path=blank_image_directory,
                                                                sample_image_path=image_directory,
                                                                sample=sample_number,
                                                                measurement=ctrl_ix,
                                                                processed_data_directory=self.parameters_dict[
                                                                    'processed_data_directory'])

                        image_scores['thickness'] = get_thickness(sample_image_directory=image_directory,
                                                                  blank_image_directory=blank_image_directory,
                                                                  dye_color=dye_color)

                        self.parameters_dict['image_directory'][sample_number][ctrl_ix] = image_directory
                        self.parameters_dict['processed_image_directory'][sample_number][ctrl_ix] = \
                            os.path.join(self.parameters_dict['processed_data_directory'],
                                         f'processed_image_s{sample_number}_m{ctrl_ix}.jpg')

                        # Convert the dictionary to a dataframe and save as CSV
                    image_scores_df = pd.DataFrame.from_dict(image_scores, orient='index')
                    image_df_transpose = image_scores_df.T
                    image_scores_list.append(image_df_transpose)

            # Combine the dataframes from each sample
            LoggingConfig.logger.info(f"{self.__class__.__name__} - Saving imaging dictionary")
            image_df_total = pd.concat(image_scores_list)
            image_scores_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                  "measurement_image_scores.csv")
            image_df_total.to_csv(image_scores_directory, index=False)
            self.parameters_dict['measurement_data']['image_scores'] = image_scores_directory

            # Take average score across measurements
            image_df_sample = image_df_total.groupby(['sample']).mean().reset_index()
            del image_df_sample['measurement']
            image_scores_directory_sample = os.path.join(self.parameters_dict['processed_data_directory'],
                                                         "sample_image_scores.csv")
            image_df_sample.to_csv(image_scores_directory_sample, index=False)
            self.parameters_dict['sample_data']['image_scores'] = image_scores_directory_sample

        except:
            LoggingConfig.logger.exception(f"{self.__class__.__name__} -  processing failed")

        self.save_parameters_dict()


class AggregateData(TaskTemplate):
    '''
    Aggregate measurement and sample data.
    '''

    def requires(self):
        yield [
            ProcessImages(),
        ]

    def run(self):

        self.get_parameters_dict()

        if len(self.parameters_dict['measurement_data']) > 0:
            measurement_df_list = []
            for measurement_directory in self.parameters_dict['measurement_data'].values():
                try:
                    measurement_df = pd.read_csv(measurement_directory)
                    measurement_df_list.append(measurement_df)
                except pd.errors.EmptyDataError as e:
                    LoggingConfig.logger.error(e)

            combined_measurement_df = reduce(lambda left, right: pd.merge(left, right, on=['sample', 'measurement']),
                                             measurement_df_list)

            LoggingConfig.logger.info(f"{self.__class__.__name__} - Saving aggregate measurement data")
            aggregate_measurement_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                           'measurement_aggregate.csv')
            combined_measurement_df.to_csv(aggregate_measurement_directory, index=False)
            self.parameters_dict['aggregate_data']['measurement'] = aggregate_measurement_directory

        if len(self.parameters_dict['sample_data']) > 0:
            sample_df_list = []
            for sample_directory in self.parameters_dict['sample_data'].values():
                try:
                    sample_df = pd.read_csv(sample_directory)
                    sample_df_list.append(sample_df)
                except pd.errors.EmptyDataError:
                    pass

            combined_sample_df = reduce(lambda left, right: pd.merge(left, right, on='sample'), sample_df_list)

            LoggingConfig.logger.info(f"{self.__class__.__name__} - Saving aggregate sample data")
            aggregate_sample_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                      'sample_aggregate.csv')
            combined_sample_df.to_csv(aggregate_sample_directory, index=False)
            self.parameters_dict['aggregate_data']['sample'] = aggregate_sample_directory

        self.save_parameters_dict()


class CustomTransformations(TaskTemplate):

    def requires(self):
        return AggregateData()

    def run(self):

        self.get_parameters_dict()

        # Generate a dataframe with the transformed variables with cost
        cost_parameters_dict = self.parameters_dict.get('cost_parameters', None)
        if cost_parameters_dict is None:
            LoggingConfig.logger.info("No cost parameters provided. Cost not calculated.")

        else:
            sample_cost_directory = os.path.join(self.parameters_dict['processed_data_directory'], 'sample_cost.csv')
            self.parameters_dict['sample_cost'] = sample_cost_directory
            cost_df = cfb.CostFunction(processing_parameters=self.parameters_dict,
                                       cost_parameters=cost_parameters_dict).cost()
            cost_df.to_csv(sample_cost_directory, index=False)

        self.save_parameters_dict()


class DataEntry(TaskTemplate):
    '''
    Data is entered at the last task in the pipeline. Parameters are fed from the bottom of the pipeline up.
    '''

    def requires(self):
        return CustomTransformations()

    def run(self):
        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self.__class__.__name__} - Complete the pipeline")

        self.save_parameters_dict()


def build_pipeline(parameter_dict):
    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    # Every luigi tasks inherits the processing parameters dictionary and the logger
    Parameters(dictionary=parameter_dict)
    LoggingConfig(campaign_results_directory=parameter_dict['campaign_results_directory'])

    pipe = luigi.build(
        [
            DataEntry(),
        ],
        local_scheduler=True,
        workers=parameter_dict['workers']
    )

    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    return pipe
