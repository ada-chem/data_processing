## Installing

```
pip install "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.bismuth_spincoating_042020&subdirectory=packages/bismuth_spincoating_042020""
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.bismuth_spincoating_042020&subdirectory=packages/bismuth_spincoating_042020"
```
