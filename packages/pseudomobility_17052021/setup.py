from setuptools import setup

PACKAGE = 'pseudomobility_17052021'
NAME = f'data_processing.{PACKAGE}'

about = {}
with open(f'data_processing/{PACKAGE}/__about__.py') as fp:
    exec(fp.read(), about)

with open("README.md", "r") as fh:
    DESCRIPTION = fh.read()

INSTALL_REQUIRES = [
    'luigi',
    'pandas',
    'numpy',
    'ada_toolbox @ git+https://gitlab.com/ada-chem/toolbox.git@master#egg=ada_toolbox',
    # 'ada_imaging.crack_detection @ git+https://gitlab.com/ada-chem/ada_imaging.git@master#egg=ada_imaging.crack_detection&subdirectory=packages/crack_detection',
    # 'ada_imaging.film_homogen @ git+https://gitlab.com/ada-chem/ada_imaging.git@master#egg=ada_imaging.film_homogen&subdirectory=packages/film_homogen',
]

setup(
    name=NAME,
    version=about["__version__"],
    install_requires=INSTALL_REQUIRES,
    namespace_packages=['data_processing'],
    packages=[NAME],
    zip_safe=False,
)
