from data_processing.data_processor.luigi_task_wrapper import *
from luigi.mock import MockFileSystem
from data_processing.data_processor.pipe_line_base_fuctions import DataEntry, CustomTransformations, AggregateData, ProcessSampleParameters, ProcessSpectroscopy, ProcessConductance, CampaignParameters, CalculatePseudomobility


class LocalCampaignParameters(CampaignParameters):
    """
    This Task sets up the data processing pipeline. This task reads in the campaign parameters JSON and converts it into
    a chemical attributes CSV.
    """


class LocalProcessConductance(ProcessConductance):
    """
    This task calculates the conductance of the given data by position.
    """

    def requires(self):
        return LocalCampaignParameters()


class LocalProcessSpectroscopy(ProcessSpectroscopy):
    """
    This task processes the spectroscopy data by position. To calculate the height of the gaussian for a sample, we must
    process 4 files: blank reflectance, blank transmission, reflectance, and transmission.
    Blank reflectance and transmission are only found in the first sample for a given campaign. As such, if we are only
    processing a single campaign, we must back track through the samples to find the blank reflectance and transmission
    data.
    """

    def requires(self):
        return LocalCampaignParameters()


class LocalCalculatePseudomobility(CalculatePseudomobility):
    """
    This task combines the spectroscopy and conductivity subpipes. Pseudomobility for a sample is calculated by getting
    the pseudomobility by position and then taking the average of the positions for the sample.
    """

    def requires(self):
        yield [
            LocalProcessSpectroscopy(),
            LocalProcessConductance()
        ]


class LocalProcessSampleParameters(ProcessSampleParameters):
    """
    Get the sample parameters json in csv format for aggregation
    """

    def requires(self):
        return LocalCampaignParameters()


class LocalAggregateData(AggregateData):
    """
    Aggregate position and sample data.
    """

    def requires(self):
        yield [
            LocalProcessSampleParameters(),
            LocalCalculatePseudomobility()
        ]


class LocalCustomTransformations(CustomTransformations):

    def requires(self):
        return [
            LocalAggregateData(),
        ]


class LocalDataEntry(DataEntry):
    """
    Data is entered at the last task in the pipeline.
    """

    def requires(self):
        # passes the dir dictionary up to the preceding task.
        return LocalCustomTransformations()


def build_pipeline(parameter_dict):
    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    # Every luigi tasks inherits the processing parameters dictionary and the logger
    Parameters(dictionary=parameter_dict)
    LoggingConfig(campaign_results_directory=parameter_dict['campaign_results_directory'])

    pipe = luigi.build(
        [
            LocalDataEntry(),
        ],
        local_scheduler=True,
        workers=parameter_dict['workers']
    )

    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    return pipe
