from data_processing.data_processor.luigi_task_wrapper import *
from luigi.mock import MockFileSystem
import shutil
import os
import pandas as pd
import data_processing.data_processor.cost_function_builder as cfb
import data_processing.data_processor.luigi_helper_functions as lhf
from functools import reduce
import numpy as np

# Imaging analysis
import warnings
from ada_imaging.film_coverage.coverage import get_coverage

# Spectroscopy analysis
from ada_toolbox.analytical_lib import analytical_tools as m

# ignore tensorflow warnings
warnings.filterwarnings('ignore', module=".*tensorflow.*")

pd.set_option('display.max_rows', 1000)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


class CampaignParameters(TaskTemplate):
    """
    This Task sets up the data processing pipeline. This task reads in the campaign parameters JSON and converts it into
    a chemical attributes CSV.
    """

    def run(self):

        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self.__class__.__name__} - Reading Campaign Parameters")

        # instantiate dict keys
        self.parameters_dict['raw_data'] = {}
        self.parameters_dict['sample_data'] = {}
        self.parameters_dict['measurement_data'] = {}
        self.parameters_dict['aggregate_data'] = {}
        self.parameters_dict['image_directory'] = {}
        self.parameters_dict['processed_image_directory'] = {}
        self.parameters_dict['sample_directories'] = {}

        # Get sample directories in the raw data folder
        campaign_files = os.listdir(self.parameters_dict['campaign_directory'])
        requested_samples = self.parameters_dict.get('sample', None)

        if "config.json" in campaign_files:
            file_directory = os.path.join(self.parameters_dict['campaign_directory'], "config.json")
            self.parameters_dict['config_directory'] = file_directory
        else:
            self.parameters_dict['config_directory'] = None

        if "campaign_parameters.json" in campaign_files:
            file_directory = os.path.join(self.parameters_dict['campaign_directory'], "campaign_parameters.json")
            self.parameters_dict['campaign_parameters_directory'] = file_directory
        else:
            self.parameters_dict['campaign_parameters_directory'] = None

        # if labels.json exists save the output
        p = os.path.join(self.parameters_dict.get('campaign_directory', ''), 'labels.json')
        if os.path.exists(p):
            labels = os.path.join(self.parameters_dict['processed_data_directory'], "labels.json")
            self.parameters_dict['labels'] = labels
            shutil.copy(p, labels)

        for file in campaign_files:

            file_directory = os.path.join(self.parameters_dict['campaign_directory'], file)

            if file.startswith('sample_') and \
                    os.path.isdir(file_directory) and \
                    len(os.listdir(file_directory)) > 1:

                file_number = int(file.split("_")[1])

                if requested_samples is None:
                    self.parameters_dict['sample_directories'][file_number] = file_directory

                else:
                    if file_number in requested_samples:
                        self.parameters_dict['sample_directories'][file_number] = file_directory
                    else:
                        pass

        self.save_parameters_dict()


class ProcessImages(TaskTemplate):
    """
    This task runs the image classification algorithms on the sample images.
    """

    def requires(self):
        return CampaignParameters()

    def run(self):

        self.get_parameters_dict()

        image_scores_list = []

        try:

            # Create a CSV of the experiment dictionary for each sample. Map JSON to DF.
            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():

                sample_number = int(sample_number)

                image_folder = os.path.join(sample_directory, 'IMAGES')

                files_in_image_folder = os.listdir(image_folder)

                image_scores = {}
                images = [file for file in files_in_image_folder if file.endswith('.jpg') and not file.startswith('blank')]
                blank_image = [file for file in files_in_image_folder if file.endswith('.jpg') and file.startswith('blank')][0]
                blank_image_directory = os.path.join(image_folder, blank_image)

                self.parameters_dict['image_directory'][sample_number] = {}
                self.parameters_dict['processed_image_directory'][sample_number] = {}

                if len(images) > 1:
                    LoggingConfig.logger.warning(
                        f"More than one image found in measurement folder: {image_folder}")

                image_id = self.parameters_dict.get('image_id', None)

                for image_number, image in enumerate(images):

                    image_directory = os.path.join(image_folder, image)


                    ctrl_ix, equip, other, position = lhf.file_name_parser(image)
                    ctrl_ix = int(ctrl_ix)

                    if image_id is not None and image_id in image or len(images) == 1:
                        image_scores['sample'] = sample_number
                        image_scores['measurement'] = ctrl_ix
                        image_scores['coverage'] = get_coverage(blank_image_path=blank_image_directory,
                                                                sample_image_path=image_directory,
                                                                sample=sample_number,
                                                                measurement=ctrl_ix,
                                                                processed_data_directory=self.parameters_dict[
                                                                    'processed_data_directory'])

                        self.parameters_dict['image_directory'][sample_number][ctrl_ix] = image_directory
                        self.parameters_dict['processed_image_directory'][sample_number][ctrl_ix] = \
                            os.path.join(self.parameters_dict['processed_data_directory'],
                                         f'processed_image_s{sample_number}_m{ctrl_ix}.jpg')

                    else:
                        image_scores['sample'] = sample_number
                        image_scores['measurement'] = ctrl_ix
                        image_scores['coverage'] = get_coverage(blank_image_path=blank_image_directory,
                                                                sample_image_path=image_directory,
                                                                sample=sample_number,
                                                                measurement=ctrl_ix,
                                                                processed_data_directory=self.parameters_dict[
                                                                    'processed_data_directory'])

                        self.parameters_dict['image_directory'][sample_number][ctrl_ix] = image_directory
                        self.parameters_dict['processed_image_directory'][sample_number][ctrl_ix] = \
                            os.path.join(self.parameters_dict['processed_data_directory'],
                                         f'processed_image_s{sample_number}_m{ctrl_ix}.jpg')

                        # Convert the dictionary to a dataframe and save as CSV
                    image_scores_df = pd.DataFrame.from_dict(image_scores, orient='index')
                    image_df_transpose = image_scores_df.T
                    image_scores_list.append(image_df_transpose)

            # Combine the dataframes from each sample
            LoggingConfig.logger.info(f"{self.__class__.__name__} - Saving imaging dictionary")
            image_df_total = pd.concat(image_scores_list)
            image_scores_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                  "measurement_image_scores.csv")
            image_df_total.to_csv(image_scores_directory, index=False)
            self.parameters_dict['measurement_data']['image_scores'] = image_scores_directory

            # Take average score across measurements
            image_df_sample = image_df_total.groupby(['sample']).mean().reset_index()
            del image_df_sample['measurement']
            image_scores_directory_sample = os.path.join(self.parameters_dict['processed_data_directory'],
                                                         "sample_image_scores.csv")
            image_df_sample.to_csv(image_scores_directory_sample, index=False)
            self.parameters_dict['sample_data']['image_scores'] = image_scores_directory_sample

        except:
            LoggingConfig.logger.exception(f"{self.__class__.__name__} -  processing failed")

        self.save_parameters_dict()


class ProcessSpectroscopy(TaskTemplate):
    '''
    This task processes the spectroscopy data by position. To calculate the height of the gaussian for a sample, we must
    process 4 files: blank reflectance, blank transmission, reflectance, and transmission.
    Blank reflectance and transmission are only found in the first sample for a given campaign. As such, if we are only
    processing a single campaign, we must back track through the samples to find the blank reflectance and transmission
    data.
    '''

    def requires(self):
        return CampaignParameters()

    def run(self):

        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self._get_classname()} - Processing Spectroscopy")

        processed_spectra_list = []
        raw_spectra_list = []

        spectroscopy_ctrl_index = self.parameters_dict.get('spectroscopy_ctrl_index', None)

        absorption_wavelength = self.parameters_dict.get('absorption_wavelength', None)
        if absorption_wavelength is None:
            absorption_wavelength = 500

        reference_absorbance = self.parameters_dict.get('reference_absorbance', None)
        if reference_absorbance is None:
            reference_absorbance = 1

        try:
            # Create a CSV of the experiment dictionary for each sample.
            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():
                sample_number = int(sample_number)
                blank_spectra = {}

                if 'UVVIS' not in os.listdir(sample_directory):
                    raise Exception('UVVIS directory doesnt exist')
                else:
                    spectra_directory = os.path.join(sample_directory, 'UVVIS')

                blank_files_all_pos = [file for file in os.listdir(spectra_directory) if
                                       'blank' in file and file.endswith('.csv')]
                blank_files_all_pos.sort()

                # determine which files to use for calculations
                if spectroscopy_ctrl_index is not None:
                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Using control index to determine which spectroscopy file to use for calculations")
                    spectra_files_all_pos = [file for file in os.listdir(spectra_directory) if file.startswith(
                        '{:03d}'.format(spectroscopy_ctrl_index)) and 'blank' not in file]
                else:
                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - No control index given - using the default file")
                    spectra_files_all_pos = [file for file in os.listdir(spectra_directory) if 'blank' in file and file.endswith('.csv')]

                spectra_files_all_pos.sort()

                # get number  of unique positions
                positions = [lhf.file_name_parser(filename)[3] for filename in spectra_files_all_pos]
                positions = list(set(positions))
                positions.sort()

                for position in positions:
                    position_number = int(position)
                    blank_spectra[position_number] = {}

                    spectra_pos = [file for file in spectra_files_all_pos if
                                   lhf.file_name_parser(file)[3] == position and file.endswith('.csv')]
                    spectra_blank_pos = [file for file in blank_files_all_pos if
                                         lhf.file_name_parser(file)[3] == position]

                    if not spectra_blank_pos:
                        raise Exception('Blank slide data NOT FOUND in sample directory')
                    elif len(spectra_pos) != len(spectra_blank_pos):
                        raise ValueError('Missmatched number of blank spectra files and sample spectra files')
                    elif len(spectra_pos) != 2:
                        raise Exception(f"Must have a single reflectance file and single transmission data file.")

                    for file in spectra_blank_pos:
                        if '_t' in file:
                            blank_transmission_directory = os.path.join(spectra_directory, file)
                            blank_transmission = pd.read_csv(blank_transmission_directory)
                            blank_transmission['sample'] = sample_number
                            blank_transmission['position'] = position_number
                            blank_transmission['r-t'] = 't'
                            blank_transmission['blank'] = True

                            blank_spectra[position_number]['t'] = blank_transmission
                        else:
                            blank_reflectance_directory = os.path.join(spectra_directory, file)
                            blank_reflectance = pd.read_csv(blank_reflectance_directory)
                            blank_reflectance['sample'] = sample_number
                            blank_reflectance['position'] = position_number
                            blank_reflectance['r-t'] = 'r'
                            blank_reflectance['blank'] = True

                            blank_spectra[position_number]['r'] = blank_reflectance

                    if blank_spectra is None:
                        raise Exception(f"Missing blank reflectance or transmission data in SAMPLE {sample_number}")

                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Processing Spectroscopy for sample {sample_number}, position {position_number}")

                    for file in spectra_pos:
                        if '_t' in file:
                            transmission_directory = os.path.join(spectra_directory, file)
                            transmission = pd.read_csv(transmission_directory)
                            transmission['sample'] = sample_number
                            transmission['position'] = position_number
                            transmission['r-t'] = 't'
                            transmission['blank'] = False
                        else:
                            reflectance_directory = os.path.join(spectra_directory, file)
                            reflectance = pd.read_csv(reflectance_directory)
                            reflectance['sample'] = sample_number
                            reflectance['position'] = position_number
                            reflectance['r-t'] = 'r'
                            reflectance['blank'] = False

                    r_0 = blank_reflectance['calibrated_signal']
                    t_0 = blank_transmission['calibrated_signal']
                    r_1 = reflectance['calibrated_signal']
                    t_1 = transmission['calibrated_signal']
                    x_nm = reflectance['x_(nm)']

                    # Calculate absorbance of film
                    absorbance_df = m.calculate_film_absorbance(r_0, t_0, r_1, t_1, x_nm)

                    # calc_absorbance_from_t_and_r
                    a_data = m.calc_absorbance_from_t_and_r(t_data=transmission, r_data=reflectance)
                    a_data = a_data.rename(
                        columns={'calibrated_signal': 'absorbance',
                                 'calibrated_signal_smoothed': 'absorbance_smoothed'})

                    abs_wave_df = pd.DataFrame({
                        'x_(nm)': [absorption_wavelength],
                        'energy_(eV)': [None],
                        'A_f': [None],
                        'log_A_f': [None],
                        'A_g': [None],
                    })

                    abs_wave_df = pd.concat([abs_wave_df, absorbance_df]).sort_values(
                        by=['x_(nm)']).reset_index(drop=True)

                    abs_wave_df = abs_wave_df.interpolate()
                    film_ab_wave = abs_wave_df[abs_wave_df['x_(nm)'] == absorption_wavelength].iloc[0][
                        'A_f']

                    relative_thickness_difference = abs(film_ab_wave - reference_absorbance) / reference_absorbance

                    spectra_dict = {
                        'sample': sample_number,
                        'measurement': position_number,
                        'film_absorption': film_ab_wave,
                        'relative_thickness_difference': relative_thickness_difference
                    }

                    # Append processed data to larger dataframe
                    processed_df = pd.DataFrame.from_dict(spectra_dict, orient='index')
                    transpose_processed_df = processed_df.T
                    processed_spectra_list.append(transpose_processed_df)

                    # Combine the data
                    pos_raw = pd.concat([blank_transmission, blank_reflectance, reflectance, transmission])
                    combined_raw = pd.merge(pos_raw, absorbance_df, on=['x_(nm)'])
                    combined_absorb = pd.merge(combined_raw, a_data, on=['x_(nm)'])

                    raw_spectra_list.append(combined_absorb)

            # Aggregate raw and processed data
            measurement_df = pd.concat(processed_spectra_list)

            # Save the data
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving spectra measurement data")
            measurement_directory = os.path.join(self.parameters_dict['processed_data_directory'],"measurement_spectra.csv")
            measurement_df.to_csv(measurement_directory, index=False)
            self.parameters_dict['measurement_data']['spectroscopy'] = measurement_directory

            # Save the sample data
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving spectra sample data")
            sample_df = measurement_df.groupby('sample').aggregate([np.mean, np.std])
            del sample_df['measurement']
            sample_df.columns = sample_df.columns.map('_'.join).str.strip('_')
            sample_df.reset_index(level=0, inplace=True)
            sample_directory = os.path.join(self.parameters_dict['processed_data_directory'],"sample_spectra.csv")
            sample_df.to_csv(sample_directory, index=False)
            self.parameters_dict['sample_data']['spectroscopy'] = sample_directory

            # Save the raw data
            raw_total = pd.concat(raw_spectra_list)
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving raw spectra data")
            raw_spectra_directory = os.path.join(self.parameters_dict['processed_data_directory'], "raw_spectra.csv")
            raw_total.to_csv(raw_spectra_directory, index=False)
            self.parameters_dict['raw_data']['spectroscopy'] = raw_spectra_directory
            del raw_total

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")

        self.save_parameters_dict()


class AggregateData(TaskTemplate):
    '''
    Aggregate measurement and sample data.
    '''

    def requires(self):
        yield [
            ProcessImages(),
            ProcessSpectroscopy(),
        ]

    def run(self):

        self.get_parameters_dict()

        if len(self.parameters_dict['measurement_data']) > 0:
            measurement_df_list = []
            for measurement_directory in self.parameters_dict['measurement_data'].values():
                try:
                    measurement_df = pd.read_csv(measurement_directory)
                    measurement_df_list.append(measurement_df)
                except pd.errors.EmptyDataError as e:
                    LoggingConfig.logger.error(e)

            combined_measurement_df = reduce(lambda left, right: pd.merge(left, right, on=['sample', 'measurement']),
                                             measurement_df_list)

            LoggingConfig.logger.info(f"{self.__class__.__name__} - Saving aggregate measurement data")
            aggregate_measurement_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                           'measurement_aggregate.csv')
            combined_measurement_df.to_csv(aggregate_measurement_directory, index=False)
            self.parameters_dict['aggregate_data']['measurement'] = aggregate_measurement_directory

        if len(self.parameters_dict['sample_data']) > 0:
            sample_df_list = []
            for sample_directory in self.parameters_dict['sample_data'].values():
                try:
                    sample_df = pd.read_csv(sample_directory)
                    sample_df_list.append(sample_df)
                except pd.errors.EmptyDataError:
                    pass

            combined_sample_df = reduce(lambda left, right: pd.merge(left, right, on='sample'), sample_df_list)

            LoggingConfig.logger.info(f"{self.__class__.__name__} - Saving aggregate sample data")
            aggregate_sample_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                      'sample_aggregate.csv')
            combined_sample_df.to_csv(aggregate_sample_directory, index=False)
            self.parameters_dict['aggregate_data']['sample'] = aggregate_sample_directory

        self.save_parameters_dict()


class CustomTransformations(TaskTemplate):

    def requires(self):
        return AggregateData()

    def run(self):

        self.get_parameters_dict()

        # Generate a dataframe with the transformed variables with cost
        cost_parameters_dict = self.parameters_dict.get('cost_parameters', None)
        if cost_parameters_dict is None:
            LoggingConfig.logger.info("No cost parameters provided. Cost not calculated.")

        else:
            sample_cost_directory = os.path.join(self.parameters_dict['processed_data_directory'], 'sample_cost.csv')
            self.parameters_dict['sample_cost'] = sample_cost_directory
            cost_df = cfb.CostFunction(processing_parameters=self.parameters_dict,
                                       cost_parameters=cost_parameters_dict).cost()
            cost_df.to_csv(sample_cost_directory, index=False)

        self.save_parameters_dict()


class DataEntry(TaskTemplate):
    '''
    Data is entered at the last task in the pipeline. Parameters are fed from the bottom of the pipeline up.
    '''

    def requires(self):
        return CustomTransformations()

    def run(self):
        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self.__class__.__name__} - Complete the pipeline")

        self.save_parameters_dict()


def build_pipeline(parameter_dict):
    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    # Every luigi tasks inherits the processing parameters dictionary and the logger
    Parameters(dictionary=parameter_dict)
    LoggingConfig(campaign_results_directory=parameter_dict['campaign_results_directory'])

    pipe = luigi.build(
        [
            DataEntry(),
        ],
        local_scheduler=True,
        workers=parameter_dict['workers']
    )

    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    return pipe
