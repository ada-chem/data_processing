## Installing

```
pip install "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.bismuth_absorption_02062020&subdirectory=packages/bismuth_absorption_02062020"
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.bismuth_absorption_02062020&subdirectory=packages/bismuth_absorption_02062020"
```
