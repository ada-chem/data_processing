## Installing

```
pip install "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.adam_haze_042022&subdirectory=packages/adam_haze_042022"
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.adam_haze_042022&subdirectory=packages/adam_haze_042022"
```
