from data_processing.data_processor.luigi_task_wrapper import *
from luigi.mock import MockFileSystem
import shutil
import os
import pandas as pd
import numpy as np
import data_processing.data_processor.cost_function_builder as cfb
import data_processing.data_processor.luigi_helper_functions as lhf
from functools import reduce
from pprint import pprint
from scipy.ndimage import measurements
from scipy import stats
import collections
import matplotlib.pyplot as plt

# Imaging analysis
import warnings

# ignore tensorflow warnings
warnings.filterwarnings('ignore', module=".*tensorflow.*")

# Image analysis pipeline
from ada_imaging.segmentation import segmentation

# Conductivity analysis
from ada_toolbox.data_analysis_lib import conductivity_analysis as cond

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


class CampaignParameters(TaskTemplate):
    """
    This Task sets up the data processing pipeline. This task reads in the campaign parameters JSON and converts it into
    a chemical attributes CSV.
    """

    def run(self):

        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self._get_classname()} - Reading Campaign Parameters")

        # instantiate dict keys
        self.parameters_dict['raw_data'] = {}
        self.parameters_dict['sample_data'] = {}
        self.parameters_dict['position_data'] = {}
        self.parameters_dict['aggregate_data'] = {}
        self.parameters_dict['image_directory'] = {}
        self.parameters_dict['sample_directories'] = {}

        # Get sample directories in the raw data folder
        campaign_files = os.listdir(self.parameters_dict['campaign_directory'])
        requested_samples = self.parameters_dict.get('sample', None)

        if "config.json" in campaign_files:
            file_directory = os.path.join(self.parameters_dict['campaign_directory'], "config.json")
            self.parameters_dict['config_directory'] = file_directory
        else:
            self.parameters_dict['config_directory'] = None

        if "campaign_parameters.json" in campaign_files:
            file_directory = os.path.join(self.parameters_dict['campaign_directory'], "campaign_parameters.json")
            self.parameters_dict['campaign_parameters_directory'] = file_directory
        else:
            self.parameters_dict['campaign_parameters_directory'] = None

        # if labels.json exists save the output
        p = os.path.join(self.parameters_dict.get('campaign_directory', ''), 'labels.json')
        if os.path.exists(p):
            labels = os.path.join(self.parameters_dict['processed_data_directory'], "labels.json")
            self.parameters_dict['labels'] = labels
            shutil.copy(p, labels)

        for file in campaign_files:

            file_directory = os.path.join(self.parameters_dict['campaign_directory'], file)

            if file.startswith('sample_') and \
                    os.path.isdir(file_directory) and \
                    len(os.listdir(file_directory)) >= 1:

                file_number = int(file.split("_")[1])

                if requested_samples is None:
                    self.parameters_dict['sample_directories'][file_number] = file_directory
                else:
                    if file_number in requested_samples:
                        self.parameters_dict['sample_directories'][file_number] = file_directory
                    else:
                        pass

        self.save_parameters_dict()


class ProcessImages(TaskTemplate):

    def requires(self):
        return CampaignParameters()

    def run(self):

        self.get_parameters_dict()
        image_scores_list = []

        LoggingConfig.logger.info(f"{self._get_classname()} - Processing XRF Images")

        try:

            # Create a CSV of the experiment dictionary for each sample. Map JSON to DF.
            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():

                LoggingConfig.logger.info(f"{self._get_classname()} - Processing sample: {sample_number}")

                try:
                    sample_number = int(sample_number)
                    if 'XRF' not in os.listdir(sample_directory):
                        raise Exception('XRF directory doesnt exist')
                    else:
                        images_directory = os.path.join(sample_directory, 'XRF')

                    image_files = os.listdir(images_directory)
                    image_files = [file for file in image_files if file == '005-xrf_10X-pos_1.bmp']

                    if len(image_files) != 1:
                        LoggingConfig.logger.info(
                            f"{self._get_classname()} - No brightfield image found for sample {sample_number}")
                        break

                    image = image_files[0]
                    image_file = os.path.join(images_directory, image)
                    image_scores = {}
                    image_scores['sample'] = int(sample_number)

                    clusters = 5

                    seg_map = segmentation.predict_image_labels(image_directory=image_file,
                                                                n_clusters=clusters,
                                                                segmentation_method='fw')

                    seg_map_dir = os.path.join(self.parameters_dict['processed_data_directory'], f"segmentation_map_{int(sample_number)}.png")
                    plt.imsave(seg_map_dir, seg_map)

                    l, w = seg_map.shape
                    total_size = l * w
                    image_scores['background_label'] = 0

                    occurences_list = seg_map.ravel().tolist()
                    occurence_dict = dict(collections.Counter(occurences_list))

                    unique_segments = range(0, clusters)
                    for unique_segment in unique_segments:

                        segment_area = occurence_dict.get(unique_segment, 0)
                        image_scores[f'C{unique_segment}_area'] = segment_area
                        image_scores[f'C{unique_segment}_area_percent'] = segment_area / total_size

                    # Convert the dictionary to a dataframe and save as CSV
                    image_scores_df = pd.DataFrame.from_dict(image_scores, orient='index')
                    image_df_transpose = image_scores_df.T
                    image_scores_list.append(image_df_transpose)

                except Exception as e:
                    LoggingConfig.logger.exception(
                        f"{self._get_classname()} - {e} -  Failed to process sample: {sample_number}")

            LoggingConfig.logger.info(f"{self._get_classname()} - Saving brightfield image data")
            image_df_total = pd.concat(image_scores_list, ignore_index=True)
            image_sample_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                  "sample_image_scores.csv")
            image_df_total.to_csv(image_sample_directory, index=False)
            self.parameters_dict['sample_data']['image_scores'] = image_sample_directory

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} -  processing failed")

        self.save_parameters_dict()


class ProcessConductivity(TaskTemplate):
    """
    This task calculates the conductance of the given data by position.
    """

    def requires(self):
        return CampaignParameters()

    def run(self):

        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self._get_classname()} - Processing Conductivity")

        raw_conductivity_list = []
        processed_conductivity_list = []
        units = {}

        try:
            # Create a CSV of the experiment dictionary for each sample. Map JSON to DF.
            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():
                sample_number = int(sample_number)

                if 'CONDUCTIVITY' not in os.listdir(sample_directory):
                    raise Exception('CONDUCTIVITY directory doesnt exist')
                else:
                    conductivity_directory = os.path.join(sample_directory, 'CONDUCTIVITY')

                LoggingConfig.logger.info(
                    f"{self._get_classname()} - Processing Conductivity for sample {sample_number}")

                conductivity_position_files = os.listdir(conductivity_directory)
                conductivity_position_files = [file for file in conductivity_position_files if file.endswith('.csv')]
                conductivity_position_files.sort()

                for file in conductivity_position_files:
                    ctrl_index, equipment, other, position = lhf.file_name_parser(file)
                    position_number = int(position)

                    LoggingConfig.logger.info(
                        f"{self._get_classname()} - Processing conductivity for sample {sample_number}, position {position_number}")

                    conductivity_file_directory = os.path.join(conductivity_directory, file)
                    conductivity_df = pd.read_csv(conductivity_file_directory)
                    conductivity_df['sample'] = sample_number
                    conductivity_df['position'] = position_number

                    # Aggregate all raw positional data
                    raw_conductivity_list.append(conductivity_df)

                    # process the conductivity data
                    # Remove saturated data points. Saturation thresholds are defined in the toolbox.
                    df_rs = cond.remove_saturated_points(data=conductivity_df)

                    # fit resistance line
                    r2, slope, intercept = cond.linear_regression_RANSAC(data=df_rs,
                                                                         x_axis="Current",
                                                                         y_axis="Voltage")

                    # check intercept is near 0
                    if slope != 0:
                        thresh = 1  # volts
                        if abs(intercept) > thresh:
                            LoggingConfig.logger.info(
                                f"{self._get_classname()} - y intercept: {intercept} > {thresh} setting to conductance to 0")
                            slope = 0

                    # convert to conductance
                    try:
                        conductance = 1 / slope
                    except ZeroDivisionError:
                        conductance = 0

                    processed_dict = {
                        'sample': sample_number,
                        'position': position_number,
                        'conductance': conductance,
                    }

                    units['r2'] = {'r2': 'siemens'}
                    units['intercept'] = {'intercept': 'siemens'}
                    units['conductance'] = {'conductance': 'siemens'}

                    processed_df = pd.DataFrame.from_dict(processed_dict, orient='index')
                    transpose_processed_df = processed_df.T
                    processed_conductivity_list.append(transpose_processed_df)

            # Combine the dataframes from each sample and save the data
            raw_conductivity_total = pd.concat(raw_conductivity_list)
            processed_conductivity_total = pd.concat(processed_conductivity_list)

            LoggingConfig.logger.info(f"{self._get_classname()} - Saving position conductivity data")
            processed_conductivity_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                            "position_conductivity.csv")
            processed_conductivity_total.to_csv(processed_conductivity_directory, index=False)
            self.parameters_dict['position_data']['conductivity'] = processed_conductivity_directory

            # Save sample conductivity
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving sample conductivity data")
            processed_sample_conductivity_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                                   "sample_conductivity.csv")
            processed_conductivity_total[['sample', 'position']] = processed_conductivity_total[
                ['sample', 'position']].astype(int)
            conductance_dicts = []
            for sample in np.unique(processed_conductivity_total['sample']):
                sample_df = processed_conductivity_total[processed_conductivity_total['sample'] == sample]
                conductance_array = np.asarray(sample_df['conductance'])
                conductance_max = np.max(conductance_array)
                conductance_mean = conductance_array.mean()
                conductance_std = conductance_array.std()
                conductance_sem = stats.sem(conductance_array)
                conductance_mean_nz = conductance_array[np.nonzero(conductance_array)].mean()
                conductance_std_nz = conductance_array[np.nonzero(conductance_array)].std()
                conductance_sem_nz = stats.sem(conductance_array[np.nonzero(conductance_array)])
                conductive_fraction = len(conductance_array[np.nonzero(conductance_array)]) / len(conductance_array)

                conductance_dict = {
                    'sample': sample,
                    'conductance_mean': conductance_mean,
                    'conductance_std': conductance_std,
                    'conductance_sem': conductance_sem,
                    'conductance_mean_nz': conductance_mean_nz,
                    'conductance_std_nz': conductance_std_nz,
                    'conductance_sem_nz': conductance_sem_nz,
                    'conductive_fraction': conductive_fraction,
                    'conductance_max': conductance_max
                }
                conductance_dicts.append(conductance_dict)

            samples_df = pd.DataFrame(conductance_dicts)
            samples_df.to_csv(processed_sample_conductivity_directory, index=False)
            self.parameters_dict['sample_data']['conductivity'] = processed_sample_conductivity_directory

            LoggingConfig.logger.info(f"{self._get_classname()} - Saving raw conductivity data")
            raw_conductivity_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                      "raw_conductivity.csv")
            raw_conductivity_total.to_csv(raw_conductivity_directory, index=False)
            self.parameters_dict['raw_data']['conductivity'] = raw_conductivity_directory

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} -  processing failed")

        self.save_parameters_dict()


class ProcessSampleParameters(TaskTemplate):
    '''
    Get the sample parameters json in csv format for aggregation
    '''

    def requires(self):
        return CampaignParameters()

    def run(self):
        self.get_parameters_dict()

        try:
            LoggingConfig.logger.info(f"{self._get_classname()} - Reformatting Sample Parameters")

            df_dicts = []

            for sample_number, sample_directory in self.parameters_dict['sample_directories'].items():
                sample_number = int(sample_number)

                if 'sample_parameters.json' in os.listdir(sample_directory):
                    with open(os.path.join(sample_directory, 'sample_parameters.json')) as json_file:
                        sample_paramers_dict = json.load(json_file)
                    realized = sample_paramers_dict.get('realized', {})
                    requested = sample_paramers_dict.get('requested', {})

                    df_dict = {}

                    for k, v in realized.items():
                        df_dict[f"{k}_realized"] = v

                    for k, v in requested.items():
                        df_dict[f"{k}_requested"] = v

                    df_dict['sample'] = sample_number
                    df_dicts.append(df_dict)

            df = pd.DataFrame(df_dicts)

            # Save the processed sample parameters
            sample_parameters_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                       'sample_parameters.csv')
            df.to_csv(sample_parameters_directory, index=False)
            self.parameters_dict['sample_data']['sample_parameters'] = sample_parameters_directory
            LoggingConfig.logger.info(f"{self._get_classname()} - Saving sample parameters data")

        except:
            LoggingConfig.logger.exception(f"{self._get_classname()} - processing failed")

        self.save_parameters_dict()


class AggregateData(TaskTemplate):
    '''
    Aggregate position and sample data.
    '''

    def requires(self):
        yield [
            ProcessImages(),
            ProcessSampleParameters(),
            ProcessConductivity()
        ]

    def run(self):

        self.get_parameters_dict()

        sample_df_list = []

        pprint(self.parameters_dict['sample_data'])

        for sample_directory in self.parameters_dict['sample_data'].values():
            try:
                sample_df = pd.read_csv(sample_directory)
                sample_df_list.append(sample_df)
            except pd.errors.EmptyDataError:
                pass

        combined_df = reduce(lambda left, right: pd.merge(left, right, on='sample'), sample_df_list)

        LoggingConfig.logger.info(f"{self._get_classname()} - Saving aggregate sample data")
        aggregate_sample_directory = os.path.join(self.parameters_dict['processed_data_directory'],
                                                  'sample_aggregate.csv')
        combined_df.to_csv(aggregate_sample_directory, index=False)
        self.parameters_dict['aggregate_data']['sample'] = aggregate_sample_directory

        self.save_parameters_dict()


class CustomTransformations(TaskTemplate):

    def requires(self):
        return AggregateData()

    def run(self):

        self.get_parameters_dict()

        # Generate a dataframe with the transformed variables with cost
        cost_parameters_dict = self.parameters_dict.get('cost_parameters', None)
        if cost_parameters_dict is None:
            LoggingConfig.logger.info("No cost parameters provided. Cost not calculated.")

        else:
            sample_cost_directory = os.path.join(self.parameters_dict['processed_data_directory'], 'sample_cost.csv')
            self.parameters_dict['sample_cost'] = sample_cost_directory
            cost_df = cfb.CostFunction(processing_parameters=self.parameters_dict,
                                       cost_parameters=cost_parameters_dict).cost()
            cost_df.to_csv(sample_cost_directory, index=False)

        self.save_parameters_dict()


class DataEntry(TaskTemplate):
    '''
    Data is entered at the last task in the pipeline.
    '''

    def requires(self):
        # passes the dir dictionary up to the preceding task.
        return CustomTransformations()

    def run(self):
        self.get_parameters_dict()

        LoggingConfig.logger.info(f"{self._get_classname()} - Complete the pipeline")

        self.save_parameters_dict()


def build_pipeline(parameter_dict):
    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    # Every luigi tasks inherits the processing parameters dictionary and the logger
    Parameters(dictionary=parameter_dict)
    LoggingConfig(campaign_results_directory=parameter_dict['campaign_results_directory'])

    pipe = luigi.build(
        [
            DataEntry(),
        ],
        local_scheduler=True,
        workers=parameter_dict['workers']
    )

    # Removes any failed/completed luigi signals from memory.
    MockFileSystem().remove('')

    return pipe
