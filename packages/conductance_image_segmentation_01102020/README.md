## Installing

```
pip install "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.conductance_image_segmentation_01102020&subdirectory=packages/conductance_image_segmentation_01102020"
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.conductance_image_segmentation_01102020&subdirectory=packages/conductance_image_segmentation_01102020"
```
