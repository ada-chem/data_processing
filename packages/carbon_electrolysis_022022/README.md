## Installing

```
pip install "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.combustion_synthesis_30072020&subdirectory=packages/combustion_synthesis_30072020"
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/data_processing.git#egg=data_processing.combustion_synthesis_30072020&subdirectory=packages/combustion_synthesis_30072020"
```
